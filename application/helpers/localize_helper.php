<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    

        
    if ( ! function_exists('heading'))
    {

        function heading($en = NULL ,$ar = NULL){
            $CI =& get_instance();
            $CI->load->library('session');
            if($CI->session->userdata('lang') == 'english' || $CI->session->userdata('lang') == ""){ return $en ; }
            else { return $ar ;}
        }
        
        
    }
    
    function styling($margin = NULL){
        if(isset($margin)) { $val = "margin-right:$margin".'px' ;}
        else { $val = "";}
            $en = "style=''";
            $ar = "style='text-align:right;direction:rtl;float:right;$val'";
            $CI =& get_instance();
            $CI->load->library('session');
            if($CI->session->userdata('lang') == 'english' || $CI->session->userdata('lang') == ""){ return $en ; }
            else { return $ar ;}
        }
        
        function conver_img($img){
            $CI =& get_instance();
            $CI->load->library('session');
            if($CI->session->userdata('lang') == 'english' || $CI->session->userdata('lang') == ""){ return $img.'.png' ; }
            else { return $img.'_ar.png' ;}
        }