<?php

    $lang['menu']='menu';
    $lang['menu_home']='Home';
    $lang['menu_about']='About';
    $lang['menu_services']='services';
    $lang['menu_products']='products';
    $lang['menu_portfolio']='work';
    $lang['menu_client']='client';
    $lang['menu_blog']='blog';
    $lang['menu_contact']='contact' ;
    $lang['client']='client' ;
    $lang['blog']='blog' ;
    $lang['portfolio']='our portfolio' ;
    $lang['contact']='contact us' ;
    
    $lang['services']='Our services' ;
    $lang['services_txt'] = 'Things we are good at';
    
    $lang['about']='about us' ;
    $lang['about_text'] = 'Be ready to know who we are';
    
    $lang['available'] = 'Our available places' ;
    $lang['available_text'] = 'we have a lot of fabulous places that you can take.' ;
    
    $lang['extra'] = 'Our extra offers' ;
    $lang['extra_text'] = 'If you sell your home with us and use our partner company for either a survey or to arrange your mortgage.' ;
    
    $lang['blog'] = 'Our blog';
    $lang['view'] =  'view';
    $lang['details'] =  'details';
    $lang['load_more'] = 'load more ..';
    $lang['read_more'] = 'read more ..';
    $lang['created_on'] = 'created on: ' ;
    $lang['contact_msg'] = 'Fill in the form below, and we will get back to you within 24 hours';
    $lang['contact_firstname'] = 'First name'; 
    $lang['contact_lastname'] = 'Last name'; 
    $lang['contact_name'] = 'name'; 
    $lang['contact_mail'] = 'email'; 
    $lang['contact_message'] = 'message'; 
    $lang['contact_comment'] = 'comment'; 
    $lang['contact_phone'] = 'phone'; 
    $lang['contact_address'] = 'Marina - Gate 5 - The Main Market - behind maxim hotel';
    $lang['contact_send']  = 'send';
    $lang['contact_text'] = 'we will be happy to hear from you soon';
    $lang['contact_line'] = 'keep in touch with us';
    $lang['contact_para'] = 'This is what just some of our happy customers have said about our services.';
    $lang['place_name'] = 'enter your name';
    $lang['place_mail'] = 'enter your mail';
    $lang['place_msg'] = 'enter your message';
    $lang['newsletter'] = 'Newsletter';
    $lang['subscription'] = 'subscription';
    $lang['articles'] = 'articles';
    
    $lang['branch_egypt'] = 'Egypt Branch';
    $lang['branch_saudi'] = 'saudi Branch';
    $lang['branch_uae'] = 'UAE Branch';
    

    $lang['all'] = 'show all';
    

    
    $lang['menu_news'] = 'news';
    
    
    // new text for slider 

    
    
    $lang['product_details'] = 'product details';
    $lang['news_line'] = 'Keep up to date with our latest news here:';
    $lang['subscribe'] =  'subscribe' ;
    $lang['our_branch'] = 'our branches';
    $lang['about_inds'] = 'about industry';
    $lang['juices'] = 'juices';
    $lang['vege'] = 'vegetables & fruits';
    $lang['fruits_jam'] = 'fruits jam';
    $lang['article_detail'] = 'The European Food Safety Authority recommends that women should drink The European Food Safety Authority recommends that women should drink about 1.6 litres of fluid and men should drink about 2.0 litres';
    $lang['article_title'] = 'Your Body Needs Water Or Other Fluids To Work Properly';
    $lang['news_head'] = ' Jam industrial food product is prepared using different types of fruit,';
    $lang['news_brief'] = 'Director and the Chief Executive Officer,jointly and severally are the only authorized spokespersons';
    $lang['times'] = 'views';
    $lang['added'] = 'added';
    $lang['Clients_inv'] = 'Clients Investments';
    $lang['clients_inv_brief'] = 'If you visit one of our stores during the first week of next month you will see our cashiers taking part in a sponsored juggle. Visit your nearest store for details';
    $lang['sight_services'] = 'Sight Services';
    $lang['sight_services_brief'] = 'If you visit one of our stores during the first week of next month you will see our cashiers taking part in a sponsored juggle. Visit your nearest store for details';
    $lang['about_kaha'] = 'About Kaha Foods';
    $lang['about_kaha_brief'] = 'If you visit one of our stores during the first week of next month you will see our cashiers taking part in a sponsored juggle. Visit your nearest store for details';
    $lang['about_brief'] = "Kaha company was founded in 1940 in the city of Kaha and started a small factory for the mobilization of fruit and vegetables a day, after more than 60 years of experience in mobilizing and food preservation, has become Kaha is a company pioneering of in the conservation of fruit and vegetables not only in Egypt, but the the Middle East, and the company now has six factories respective integrated production lines (Kaha Liberation in 1961, cob 1965, the Abu Kabir in 1971,";
    
    $lang['news-blogs'] = "News & Blogs";
    $lang['news-text'] = 'Be updated with our latest news and activity';
    $lang['admin'] = 'Posted by Admin';
    $lang['tags'] = 'Development, Construction';
    $lang['comments'] = 'Comments' ;
    $lang['leave-comment'] = 'Post your comments here';
    $lamg['comment-alert'] = 'Your email address will not be published. Required fields are marked *';
    $lang['news-updates'] = 'latest news and updates';
    $lang['news-updates-test'] = 'no-obligation valuation on your property at a time of day thatâ€™s suitable for you.';
    
    #side bar 
    $lang['prices'] = 'prices';
    $lang['act_name'] = 'service name ';
    $lang['act_cost'] = 'service cost';
    $lang['play-bowling'] = 'how to play bowling ?';
    $lang['kids-area'] = 'kids area';
    $lang['champ'] = 'championships';
    
    # footer 
    $lang['Questions'] = 'Have Any Questions';
    $lang['footer_contact_text'] = 'Don\'t Hesitate to contact us and get the solution';
    $lang['signup'] = 'SignUp';
    
    $lang['Contact_info'] = 'Contact Info';
    $lang['links'] = 'useful links' ;
    $lang['footer_about'] = 'about El Zeat Group';
    $lang['footer_about_text'] = 'We are excited to launch our new company and product Ooooh. After being featured in too many magazines to mention and having created an online stir, we know that Ooooh is going to be big. You may have seen us in the Dinosaursï؟½ Den where we were we told that we didnï؟½t need them because we were already doing it so well ourselves, so thatï؟½s what we have continued to do.';
    
    
    #work 
    $lang['All'] = 'All projects';
    $lang['property'] = 'property';
    
    $lang['General'] = 'General Contracting' ;
    $lang['service_text1'] = 'Real estate development requires that you investigate the possible negative and positive outcomes of a real estate project before you start investing time and money into it,This analysis allows you to take into consideration legal, economic, technological, and other factors, which are important for the successful completion of real estate development..';
    $lang['service_text2'] = 'Project Marketing consultation & evaluation
                                We specialize in working at both strategic and operational levels to build the capability of teams to systematically plan, measure,
                                and report their results and to tell their performance story. We do this by collaboratively developing and using a practical evidenced-based approach to show the teamâ€™s contribution to the organizationâ€™s strategic and/or operational outcomes and to gather evidence to inform strategic and operational decisions..';
    
    
    $lang['service_menu1'] = 'Real Estate Development Feasibility study';
    $lang['service_menu2'] = 'Construction Management';
    $lang['service_menu3'] = 'Marketing & Sales planning and budgeting';
    $lang['service_menu4'] = 'Real Estate Market Research & Analysis';
    $lang['service_menu5'] = 'Real Estate Investments';
    $lang['service_menu6'] = 'Structure Remodeling';
    
    
    $lang['events'] = 'events organising ';
    $lang['events-text'] = 'Our highly educated staff will make sure that your satisfication will be gained on time and perfect quality .';
    
    $lang['service'] = 'Our services';
    $lang['service-text'] = 'Center seeks along with bowling to offer some of the Games Entertainment in order';
    
    $lang['about-text'] = 'With the participation of 80 countries, it is the first world championship
                                    Held in the Arab Republic of Egypt.' ;
    
    #news 
    $lang['recentposts'] = 'Recent Posts';
    $lang[''] = '';
    $lang['search'] = 'Search Here' ;
    
    
    $lang['contactemail'] = 'Contact us via email';
    $lang['emailtext'] = 'If you have any questions, you can contact us via email.';
    
    $lang['findus'] = 'Where to find us';
    $lang['findusaddress'] = 'We are located in Salah Salem Rd. - Behind 6th of Oct. Panorama - Cairo';
    
    
    #slide 1
    $lang['quote1'] = 'International  <span>Bowling</span> Center';
    $lang['quote2'] = 'the Egyptian center for the armed forces.';
    
    //  mediar center 
    $lang['mediacenter'] = 'Media Center';
    $lang['viewphotos'] = 'View all photos';
    
    // contact 
    $lang['get_quote'] = 'get quote ';
    $lang['get_quote_text'] = 'Providing professional entertainment services .';
    $lang['contact_text'] = 'Have a question for us? Please fill in the form below and we’ll respond in 24 hours.';
    $lang['fullname'] = 'Full Name:';
    $lang['youremail'] = 'Your Email address';
    $lang['yourmessage'] = 'Your message (Optional):';
    $lang['tel'] = 'Tel:';
    $lang['centerinfo'] = 'center info';
    
    $lang['center_photos'] = 'center photos';
    $lang['mediacenter'] = 'Media Center';
    $lang['All'] = 'All';
    $lang['menu_media'] = 'Media';
    
    //news 
    $lang['here'] = 'You are here';
    $lang['centernews'] = 'center <span>News</span>';
    $lang['address'] = 'Address';
    $lang['days-period'] = 'Sat-Fri';
    $lang['add_text'] = 'Salah Salem Rd. - Behind 6th of Oct. Panorama - Cairo';
    
    //sidebar 
    $lang['latest_news'] = 'Latest News';
    $lang['news_details'] = 'News Details';
    $lang['bowlingcenter'] = 'Bowling Center';
    $lang['Brochure'] = 'Brochure download';
    
    // footer 
    $lang['copyrights'] = '<strong>© IBC egypt</strong> 2016. All rights reserved';