<?php

    $lang['menu']='القائمة';
    $lang['menu_home']='الرئـيـسـيـة';
    $lang['menu_about']='عـن المركز';
    $lang['menu_about2']='ميديا أر - عن الشركة - عملاءنا - الإتصال بنا الرئيسبة - ';
    $lang['menu_services']='خـدمـاتـنـا';
    $lang['menu_products']='منتجـاتـنا';
    $lang['menu_portfolio']='أعـمالنـا';
    $lang['menu_client']='عـمـلائنـا';
    $lang['menu_analysis']='التحاليل';
    $lang['menu_support'] = 'الدعم';
    $lang['menu_team_work']='فريق العمل';
    $lang['menu_privacy']='سياسة الخصوصية';
    $lang['contract_privacy']='سياسة التعاقداتللمؤسسات والمراكز البحثية';
    $lang['menu_blog']='المـدونـة';
    $lang['menu_contact']='الإتـصـال بـنـا ' ;
    $lang['client']='client' ;
    $lang['blog']='blog' ;
    $lang['portfolio']='our portfolio' ;
    $lang['contact']='contact us' ;
    $lang['services']='services' ;
    $lang['about']='about' ;
    
    $lang['blog'] = 'المدونــة';
    $lang['view'] =  'عرض';
    $lang['details'] =  'تفاصيل';
    $lang['load_more'] = 'عرض المزيد ..';
    $lang['read_more'] = 'قراءة المزيد..';
    $lang['created_on'] = 'تم الإضافة' ;
    $lang['next'] = 'التالى';
    $lang['contact_msg'] = 'راسلنا عبلا البريد الإلكترونى وسنتواصل معك فى خلال يوم';
    $lang['contact_name'] = 'الأسم'; 
    $lang['contact_mail'] = 'البريد الإلكترونى'; 
    $lang['contact_phone'] = 'التليفون'; 
    $lang['contact_message'] = 'الرسالة'; 
    $lang['contact_send']  = 'ارسال';
    $lang['place_name'] = 'الأسـم';
    $lang['place_mail'] = 'البريد الإلكترونى';
    $lang['place_msg'] = 'اترك رسالتك هنا';
    $lang['newsletter'] = 'اشترك';
    $lang['subscription'] = 'الإشتراك فى بريد الموقع';
    $lang['articles'] = 'مقالات';
    

    
    $lang['menu_news'] = 'أخر الأخبار';
    
    /* header */
    $lang['search'] = 'بحث';    
    $lang['header_login'] = 'تسجيل الدخـــــول ';
    $lang['individuals'] = 'أفراد | مؤسسات ';
    $lang['analysis_results'] = 'نتائج التحاليل';
    $lang['home_visit'] = 'زيارات منزلية';
    $lang['get_branch'] = 'حدد أقرب الفروع';
    
    
    

    $lang['all'] = 'عرض الكل';
    $lang['times'] = 'مشاهدات';
    $lang['added'] = 'إضافة';
    $lang['extra'] = 'المزيد';
    
    $lang['fast_contact'] = 'اتصال سريع';
    $lang['make_order'] = 'عمل طلب سريع';
    $lang['career'] = 'التوظيف';
    
    
    /* branches */
    $lang['governate'] = 'المحافظة';
    $lang['city'] = 'الحى / المدينة  ';
    $lang['show_location'] = 'عرض بيانات الفرع';
    $lang['branches_text'] = 'اختر المدينة او المنطقةالمفضلة اليك وسنرشدك الى اقرب فرع اليك';
    $lang['choose'] = 'أختـر';
    $lang['nearest_location'] = 'أقرب فرع';
    
    /*  home page */
    $lang['lab_services'] = 'خدمات المعمل';
    $lang['recommend'] = 'نصائح طبيةهامة';
    $lang['precautions'] = 'احتياطات التحاليل';
    $lang['patients'] = 'خدمات المرضى';
    $lang['about_cetris'] = 'عن سيترس' ;
    $lang['media_cetris'] = 'سيترس ميديا';
    
    
    $lang['contactemail'] = 'الاتصال بنا عبر البريد الإلكترونى';
    $lang['emailtext'] = 'اذا كان لديك اى ستفسارات او اقتراحات ، لا تترد فى مراسلتنا ';
    
    $lang['findus'] = 'كيفية الوصول الينا';
    $lang['findusaddress'] = 'موقعنا على طريق صلاح سالم - خلف بانوراما اكتوبر ';
    
    /* slider */
    $lang['quote1'] = 'المركز <span> الدولى </span> للبولينج';
    $lang['quote2'] = 'مركز البولينج المصرى  للقوات المسلحة';
    
    //  mediar center 
    $lang['mediacenter'] = 'مركز  الميديا';
    $lang['viewphotos'] = 'عرض جميع الصور';

    // contact 
    $lang['get_quote'] = 'استعلام ';
    $lang['get_quote_text'] = 'افضل مركز ترفيهى تستطيع قضاء العطله فيه مع الأصدقاء';
     $lang['contact_text'] = 'اذا كان عندك اى استفسار .. ارسل لنا رسالتك وسنرد عليك فى خلال 24 ساعه ';
    $lang['fullname'] = 'الأسم بالكامل';
    $lang['youremail'] = 'البريد الإلكترونى';
    $lang['yourmessage'] = 'رسالتك  :';
    $lang['tel'] = 'التيليفون:';
    $lang['centerinfo'] = 'بيانات المركز';
    
    $lang['comments'] = 'التعليقات' ;
    $lang['leave-comment'] = 'اترك تعليقك هنا';
    
    
    $lang['events'] = 'تنظيم الحفلات';
    $lang['events-text'] = 'فإن موظفينا المتعلمين تعليما عاليا تأكد من أن الارتياح الخاص بك سيتم اكتسبت في الوقت المحدد ونوعية ممتازة.';
    
    $lang['service'] = 'خدماتنا';
    $lang['service-text'] = 'يسعى مركز جنبا إلى جنب مع البولينج لتقديم بعض الألعاب والترفيه من أجل';
    
    $lang['about-text'] = 'بمشاركة 80 دولة، وهو أول بطولة عالمية
                                     الذي عقد في جمهورية مصر العربية.' ;
    
    //news 
    $lang['here'] = 'انت الان';
    $lang['centernews'] = 'أخبار <span>المركز</span>';
    $lang['address'] = 'العنوان';
    $lang['days-period'] =' السبت - الجمعة';
    $lang['add_text'] = 'طريق صلاح سالم - خلف بانوراما اكتوبر - القاهرة ';
    //sidebar 
    $lang['latest_news'] = 'أخر الأخبار';
    $lang['news_details'] = 'تفاصيل الخبر ';
    $lang['bowlingcenter'] = 'مركز البولينج';
    $lang['Brochure'] = 'تحميل كتيب المركز';
    
    #side bar 
    $lang['prices'] = 'الأسعار';
    $lang['act_name'] = 'السعر';
    $lang['act_cost'] = 'البيان';
    $lang['play-bowling'] = 'كيف تلعب البولبنج ?';
    $lang['kids-area'] = 'منطقة الأطفال';
    $lang['champ'] = 'بطولات المركز';
    
    
    // footer 
    $lang['copyrights'] = '<strong>© مركز البولينج المصرى</strong> 2016. جميع الحقوق محفوظه';
    
   /* news page */
    $lang['latest_news'] = 'أخر الأخبار';
    $lang['news_head_text'] = 'تفقد أخر الأخبار الخاصة بسيترس';
    $lang['read'] = 'قراءة';
    $lang['related_topics'] = 'مواضيع ذات صلة';
    $lang['sharing'] = 'منتجاتنا';
    $lang['news_preca'] = 'عملية التصنيع';
    $lang['news_online_ser'] = 'مكونات المنتج';
    $lang['news_contract'] = 'لماذا سيترس؟';
    $lang['news_why_life'] = 'لماذا سيترس ؟';
    $lang['news_closest'] = 'توصل لاقرب فرع اليك';
    
    
    $lang['furniture'] = 'الأثاث';
    $lang['carpets'] = 'السجاد';
    $lang['toilets'] = 'الحمام و البانيو';
    $lang['description_text'] = 'رش رغوة CETRIS مباشرة على السطوح، وترك لمدة 5 إلى 10 دقائق السماح رغوة بحل البقع
ثم شطف أو يمسح بقطعة قماش مبللة على السجاد والأقمشة،';
    

    
    /* users login page */
    $lang['users_login'] = 'تسجيل الدخول لحسابك الشخصى';
    $lang['username'] = 'اسم المستخدم';
    $lang['password'] = 'كلمة المرور';
    $lang['login'] = 'تسجيل الدخول';
    $lang['login_instruction'] = 'تعليمات';
    $lang['have_account'] = 'لا أملك حساباً';
    
    $lang['support_title'] = ' الدعم';
    $lang['support_text'] = 'ما يمكن ان نوفره لعملائنا';
    

    $lang['center_photos'] = 'صور المركز';
    $lang['mediacenter'] = 'المركز الإعلامى';
    $lang['menu_media'] = 'ميديا';
    $lang['All'] = 'عرض الكل';
    
    
     /* contact page */
    $lang['contact_head_title'] = 'يهمنا توفير كل وسائل الإتصال تيسيرا على العملاء امكانية الوصول و التواصل معنا فى اى وقت .' ;
    $lang['contact_paragraph'] = 'فريق خدمة العملاء لدينا على استعداد لاستقبال رسائلكم الألكترونية والرد علي جميع طلباتكم واستفساراتكم' ;
    $lang['contact_branch'] = 'لتحديد اقرب فرع اليك فقط اختر المدينة او الحى المقرب لك' ;
    $lang['branches'] = 'فروعنا ' ;
    $lang['contact_brief'] = 'فى انتظار اتصالاتكم واستفساراتكم';
    $lang['home_visit_title'] = 'خدمة سيترس المنزلية' ;
    $lang['home_visit_text'] = 'نبذة مختصرة عن معامل سيترس';
    
        /* footer  */
    $lang['subscribe'] = 'اشترك';
    $lang['subscribe_text'] = 'اشترك فى موقعنا حتى يصلك كل جديد وتحديث فى موقعنا';
    $lang['footer_brief'] = 'نبذة عن الشركة';
    $lang['footer_sitemap'] = 'خريطة الموقع';
    $lang['footer_contact'] = 'للإتصال';
    $lang['footer_about'] = 'CETRIS هو منتج قوي جديد في إطلاقه في مصر، وهو منظف متعدد الأغراض، والمصممة لجميع احتياجات المنزل، لديها تأثير قوي على التنظيف، والمصممة لجميع احتياجات المنزل، لديها تأثير قوي على التنظيف';
    $lang['fooetr_location'] = 'اذا كنت فى مكان قريب وتحتاج لأقرب فرع من معاملنا فقط ، اترك بياناتك فى استمارة الحجز وسيقوم فريق الدعم بالتنسيق معك';
    $lang['address'] = 'العنوان';
    $lang['address_text'] = ' طريق طه حسين - النزهة الجديدة - القاهرة - مصر';
    $lang['mobile'] = 'الموبايل';
    $lang['telephone'] = 'التليفون';
    $lang['copy_rights'] = 'جميع الحقوق محفوظة  لسيترس © ' ;
    $lang['follow'] = 'تابعونا';
    $lang['know_about'] = 'أعرف أكثر عن سيترس';
    $lang['activity'] = 'مواضيع ذات صلة' ;
    $lang['support'] = 'الدعم';
    $lang['people_say'] = 'اراء العملاء!';
    $lang['values'] = 'مميزات المنتج';
    $lang['history'] = 'تاريخ المنتج';
    $lang['product_usage'] = 'مواضع الإستخدام';