


    <!-- *************************************************************  begin of page contents ********************************************************** -->
    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li>
                  <p>انت الأن</p>
                </li>
                <li><a href="#" class="active">عرض جميع الإتحادات</a> </li>
            </ul>
            <div class="clearfix"></div>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>عرض - <span class="semi-bold">جميع الإتحادات</span></h3>
            </div>
            <div class="page-title" style="margin-bottom: 30px;"> 
                <a href="<?php echo base_url(); ?>federations/add_federation"><button class="btn btn-success " style="padding: 10px 35px;"> <i class="fa fa-plus" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> إضافة اتحاد جديد </button> </a>
            </div>
            <div style="background: #fff; padding: 20px; margin: 20px 0px;" class="row">
                <?php echo validation_errors('<p class="alert alert-danger">','</p>'); ?>
                <?php echo form_open('federations/get_filter_data'); ?>
                <div class="col-md-7">
                    <div class="radio radio-success" style="margin-top:5px;">
                        <input id="name" type="radio" name="searchtype" value="fed_name" checked="checked">
                        <label for="name">الإسم</label>
                        <input id="phone" type="radio" name="searchtype" value="fed_phone">
                        <label for="phone">التليفون</label>
                        <input id="email" type="radio" name="searchtype" value="fed_mail">
                        <label for="email">البريد الإلكترونى</label>
                 
                    </div>
                </div>
                <div class="col-md-3">
                    <input type="text" name="search" style="vertical-align:middle;font-family: 'Open Sans' !important;"/>
                </div>

                <div class="col-md-1">
                    <button class="btn btn-success" type="submit" name="post-search" style="padding:8px 30px;">  <i class="fa fa-search"></i>  بحــث</button>
                </div>
                <?php echo form_close(); ?>
            </div>
                <?php if(isset($message) && $message == TRUE){ echo $message ;} ?>
                 <?php if(isset($results)){ ?>
                    <div style="background: #ECF6CE;padding: 10px 5px 2px 15px; margin: -15px 0px 15px 0px" class="search_results">
                        <p style="margin-right:15px;"> نتائج البحـــث :  <span style="font-size: 21px;font-weight: bold; border-radius: 50%;display: inline-block; margin:0px 10px;"> <?php echo $results; ?></span> عدد النتائج الموجودة    
                            <a style="margin-right: 30%;" href="<?php echo base_url();?>federations/list_federations">| الرجوع للقائمة الرئيسية</a></p>
                    </div>        
                <?php }?>
                <?php if(isset($status) && !empty($status)){ echo $status; } ?>
            
            
            
            <div class="clearfix"></div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="grid simple ">
                                    <div class="grid-title no-border">
                                        <h4>قائمة   <span class="semi-bold">أسماء الإتحادات </span></h4>
                                    </div>
                                    <div class="grid-body no-border">
                                        <table class="table table-bordered no-more-tables">
                                            <thead>
                                                <tr>
                                                    <th style="width:1%"> مسل </th>
                                                    <th class="text-center" style="width:20%">اسم الإتحاد </th>
                                                    <th class="text-center" style="width:16%">الشعار </th>
                                                    <th class="text-center" style="width:12%"> رقم التليفون </th>
                                                    <th class="text-center" style="width:14%">البريد الإلكترونى </th>
                                                    <th class="text-center" style="width:6%">تعديل</th>
                                                    <th class="text-center" style="width:6%">حذف</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php $n=1; if(count($rows)){foreach ($rows as $r) { ?>
                                                    <td style="vertical-align:middle;font-family: 'Open Sans' !important;"> <?php echo $n++; ?></td>
                                                    <td class="text-center" style="vertical-align:middle"><?php echo $r->fed_name;?></td>
                                                    <td class="text-center" style="vertical-align:middle">
                                                        <div class="player-personal-photo">
                                                            <img src="<?php if(strlen($r->photo) > 0){ echo base_url().'source/'.$r->photo;} else {echo base_url().'images/gravatar.png';} ?>" alt="" width="70"/>
                                                        </div>
                                                    </td>
                                                    <td class="text-center" style="vertical-align:middle;font-family: 'Open Sans' !important;"><?php echo $r->fed_phone;?></td>
                                                    <td class="text-center" style="vertical-align:middle;font-family: 'Open Sans' !important;"><?php echo $r->fed_mail;?></td>
                                                    <td class="text-center" style="vertical-align:middle">
                                                        <a href="<?php echo base_url(); ?>federations/edit_federation/<?php echo $r->fed_id;?>"><i class="fa fa-edit" style="font-size: 24px;"> </i></a>
                                                    </td>
                                                    <td class="text-center" style="vertical-align:middle">
                                                        <a href="<?php echo base_url(); ?>federations/delete_federation/<?php echo $r->fed_id;?>"><i class="fa fa-trash" style="font-size: 24px;"> </i></a>
                                                    </td>
                                                </tr>
                                                <?php }} else { echo '<p class="alert alert-warning"> there is no data to be rendered </p>';} ?>
                                            </tbody>
                                        </table>
                                    </div>
				</div>
                            </div>
                    </div>    
                        
                        

         
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
