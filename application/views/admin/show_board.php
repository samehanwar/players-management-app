    <div class="modal fade" id="myModal2" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title"> wrong number </h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="myModal3" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title"> wrong number </h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="page-content"> 
        <div class="clearfix"></div>
            <div class="content" >  
                <ul class="breadcrumb">
                    <li>
                      <p>انت الأن</p>
                    </li>
                    <li><a href="#" class="active">  لعبة البولينج</a></li>
                </ul>
                <div class="clearfix"></div>
                <div class="page-title"> <i class="icon-custom-left"></i>
                    <div class="page-title" style="margin-bottom: 30px;display: inline-block; margin-top: 10px;"> 
                        <a href=""><button class="btn btn-default" style="padding: 5px 35px;"> <i class="fa fa-calculator" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> حساب لعبة البولينج </button> </a>
                    </div>
                </div>
                    <?php if(isset($status) && !empty($status)){ echo $status; } ?>

                <div class="row" >
                    <div class="col-md-12" id="add-data">
                        <div class="dashboard-bowling">
                            <div class="frame-buttons" id="frame-buttons">
                                <button class="btn btn-info" data-value="1"> 1 </button>
                                <button class="btn btn-info" data-value="2"> 2 </button>
                                <button class="btn btn-info" data-value="3"> 3 </button>
                                <button class="btn btn-info" data-value="4"> 4 </button>
                                <button class="btn btn-info" data-value="5"> 5 </button>
                                <button class="btn btn-info" data-value="6"> 6 </button>
                                <button class="btn btn-info" data-value="7"> 7 </button>
                                <button class="btn btn-info" data-value="8"> 8 </button>
                                <button class="btn btn-info" data-value="9"> 9 </button>
                                <button class="btn btn-info" data-value="0"> 0 </button>
                                <span style="margin: 0px 25px;"> | </span>
                                <button class="btn btn-info" data-value="X"> X </button>
                                <button class="btn btn-info" data-value="/"> / </button>
                                <button class="btn btn-info" data-value="F"> F </button>
                                <button class="btn btn-info" data-value="O"> O </button>
                            </div>
                            <?php echo form_open('bowling_calculate/calculate'); ?>
                            <div class="table-responsive" id="calcbowling">
                                <table class="table" style="text-align:left;font-family:'Open Sans'!important;">
                                    <tr>  
                                        <td id="first" class="first">
                                            <div class="frame-box">
                                                <label> F1 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F1-1" id="frame1" class="frame1" name="F11" value="<?php if(isset($F11)) echo $F11; ?>"/>
                                                    <input type="text" class="form-control" data-roll="F1-2"        class="frame1"     name="F12" value="<?php if(isset($F12)) echo $F12; ?>"/>
                                                    <input type="hidden" class="form-control" name="frame1" value="F1"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F1)) echo $F1; ?>" />
                                            </div>
                                        </td> 
                                        <td>
                                            <div class="frame-box">
                                                <label> F2 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F2-1" name="F21" value="<?php if(isset($F21)) echo $F21; ?>"/>
                                                    <input type="text" class="form-control" data-roll="F2-2" name="F22" value="<?php if(isset($F22)) echo $F22; ?>"/>
                                                    <input type="hidden" class="form-control" name="frame2" value="F2"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F2)) echo $F2; ?>" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F3 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F3-1" name="F31" value="<?php if(isset($F31)) echo $F31; ?>" />
                                                    <input type="text" class="form-control" data-roll="F3-2" name="F32" value="<?php if(isset($F32)) echo $F32; ?>" />
                                                    <input type="hidden" class="form-control" name="frame3" value="F3"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F3)) echo $F3; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F4 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F4-1" name="F41" value="<?php if(isset($F41)) echo $F41; ?>"/>
                                                    <input type="text" class="form-control" data-roll="F4-2" name="F42" value="<?php if(isset($F42)) echo $F42; ?>"/>
                                                    <input type="hidden" class="form-control" name="frame4" value="F4"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F4)) echo $F4; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F5 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F5-1" name="F51" value="<?php if(isset($F51)) echo $F51; ?>" />
                                                    <input type="text" class="form-control" data-roll="F5-2" name="F52" value="<?php if(isset($F52)) echo $F52; ?>" />
                                                    <input type="hidden" class="form-control" name="frame5" value="F5"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F5)) echo $F5; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F6 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F6-1" name="F61" value="<?php if(isset($F41)) echo $F41; ?>" />
                                                    <input type="text" class="form-control" data-roll="F6-2" name="F62" value="<?php if(isset($F41)) echo $F41; ?>" />
                                                    <input type="hidden" class="form-control" name="frame6" value="F6"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F6)) echo $F6; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F7 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F7-1" name="F71" value="<?php if(isset($F71)) echo $F71; ?>" />
                                                    <input type="text" class="form-control" data-roll="F7-2" name="F72" value="<?php if(isset($F72)) echo $F72; ?>" />
                                                    <input type="hidden" class="form-control" name="frame7" value="F7"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F7)) echo $F7; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F8 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F8-1" name="F81" value="<?php if(isset($F81)) echo $F81; ?>" />
                                                    <input type="text" class="form-control" data-roll="F8-2" name="F82" value="<?php if(isset($F82)) echo $F82; ?>" />
                                                    <input type="hidden" class="form-control" name="frame8" value="F8"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F8)) echo $F8; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F9 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F9-1" name="F91" value="<?php if(isset($F91)) echo $F91; ?>" />
                                                    <input type="text" class="form-control" data-roll="F9-2" name="F92" value="<?php if(isset($F92)) echo $F92; ?>" />
                                                    <input type="hidden" class="form-control" name="frame9" value="F9"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F9)) echo $F9; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label> F10 </label>
                                                <div class="score-fields">
                                                    <input type="text" class="form-control" data-roll="F10-1" name="F101" value="<?php if(isset($F101)) echo $F101; ?>" />
                                                    <input type="text" class="form-control" data-roll="F10-2" name="F102" value="<?php if(isset($F102)) echo $F102; ?>" />
                                                    <input type="hidden" class="form-control" name="frame10" value="F10"/>
                                                </div>
                                                <input type="text" class="form-control" value="<?php if(isset($F10)) echo $F10; ?>"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="frame-box">
                                                <label style="text-align: center;"> TOTAL SCORE </label>
                                                <div class="btn btn-default" id="totalscore" type="button" style="padding:40px 60px;background: #F0F0EE;font-family:'Open Sans'!important;font-weight: bold;font-size: 24px;"> &nbsp;  </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                            <button class="btn btn-danger btncalc"> <i class="fa fa-calculator"></i> calculate </button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div> <!-- end of content -->
            </div> <!-- end of page-content -->
    

