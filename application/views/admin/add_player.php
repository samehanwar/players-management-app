    <div class="modal fade" id="myModal" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">image manager</h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content" >  
            <ul class="breadcrumb">
                <li>
                  <p>انت الأن</p>
                </li>
                <li><a href="#" class="active">اضافة لاعب جديد</a> </li>
            </ul>
            <div class="clearfix"></div>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <div class="page-title" style="margin-bottom: 30px;display: inline-block; margin-top: 10px;"> 
                <a href="<?php echo base_url(); ?>players/add_player"><button class="btn btn-success " style="padding: 5px 35px;"> <i class="fa fa-plus" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> إضافة لاعب جديد </button> </a>
            </div>
            <div class="page-title" style="margin-bottom: 30px;display: inline-block; margin-right: 20px; margin-top: 10px;"> 
                <a href="<?php echo base_url(); ?>players/list_players"><button class="btn btn-success " style="padding: 5px 35px;"> <i class="fa fa-plus" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> العودة لقائمة اللاعبين</button> </a>
            </div>
        </div>
            <?php if(isset($status) && !empty($status)){ echo $status; } ?>
            <div class="page-title">  </div>	
            <?php if(isset($rows)) { foreach ($rows as $r){ } }?>
            <?php if(!isset($rows)){ echo form_open_multipart('players/create_player');} else{echo form_open_multipart('players/edit_player/'.$r->player_id);} ?>
                <div class="row" >
                    <div class="col-md-12">
                      <ul class="nav nav-tabs" id="myTab" >
                        <li class="active pull-right" style="float: right !important;"><a href="#tab5hellowWorld" data-toggle="tab" style="padding: 15px 35px;font-size: 19px;">بيانات اللاعب </a></li>
                        <li class="pull-right" style="float: right !important;"><a href="#tab5FollowUs" data-toggle="tab" style="padding: 15px 35px;font-size: 19px;">بيانات أخرى</a></li>
                        <li class="pull-right" style="float: right !important;"><a href="#tab5photos" data-toggle="tab" style="padding: 15px 35px;font-size: 19px;">معرض الصور</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab5hellowWorld">
                          <div class="row column-seperation">
                            <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    <form class="form-no-horizontal-spacing" id="form-condensed">	
                        <div class="row column-seperation">
                            <div class="col-md-6">
                                <h4>البيانات الشخصية</h4>            
                                <div class="row form-row">
                                  <div class="col-md-5">
                                      <input name="player_fname" id="form3FirstName" type="text"  class="form-control" placeholder="الإسم الأول" value="<?php if(isset($r->player_fname)){ echo $r->player_fname; }?>">
                                  </div>
                                  <div class="col-md-7">
                                    <input name="player_lname" id="form3LastName" type="text"  class="form-control" placeholder="الإسم الأحير" value="<?php if(isset($r->player_lname)){ echo $r->player_lname; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-5">
                                        <select name="nationality" id="form3Gender" class="select2 form-control">
                                            <?php if(isset($rows)){ ?>
                                            <option value="<?php if(isset($r->nationality)){ echo $r->nationality; }?>"><?php if(isset($r->nationality)){ echo $r->nationality; }?></option>
                                            <?php } ?>
                                            <?php include_once 'nationalities.php'; ?>
                                        </select>
                                  </div>
                                  <div class="col-md-7">
                                    <input type="text" placeholder="تاريح الميلاد" class="form-control" style="font-family:'Open Sans'!important;" id="form3DateOfBirth" name="dateofbirth" value="<?php if(isset($r->dateofbirth)){ echo $r->dateofbirth; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-12">
                                        <input type="text" placeholder="الوظيفه" class="form-control" id="form3DateOfBirth" name="occupation" value="<?php if(isset($r->occupation)){ echo $r->occupation; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-8">
                                    <div class="radio">
                                        <?php if(!isset($r->gender)){  ?>
                                            <input id="male" type="radio" name="gender" value="male" checked="checked">
                                            <label for="male">ذكـر</label>
                                            <input id="female" type="radio" name="gender" value="female">
                                            <label for="female">أنثى</label>
                                        <?php }?>
                                        <?php if(isset($r->gender)){  ?>
                                            <input id="male" type="radio" name="gender" value="male" <?php if(isset($r->gender) && $r->gender == 'male'){ echo 'checked="checked"'; ?> <?php }?>>
                                            <label for="male">ذكـر</label>
                                            <input id="female" type="radio" name="gender" value="female" <?php if(isset($r->gender) && $r->gender == 'female'){ echo 'checked="checked"'; ?> <?php }?>>
                                            <label for="female">أنثى</label>
                                        <?php }?>
                                    </div>
                                  </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <input name="email" id="form3Email" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="البريد الإلكترونى" value="<?php if(isset($r->email)){ echo $r->email; }?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div  style="margin-right: 40px;">
                                    <h4>الصورة الشخصية</h4>
                                    <table>
                                        <tbody>
                                            <input type="hidden" id="fieldup" value="" />
                                                <tr class="meta-bordered image-data"> 
                                                    <td> 
                                                        <button type="button"  class="btn btn-default btn-lg image_btn" data-toggle="modal" data-target="#myModal">
                                                            <img src="<?php if(isset($r->photo) && strlen($r->photo) > 0 ){ echo base_url().'source/'.$r->photo;}  else {echo base_url().'images/gravatar.png';} ?>" class="img-responsive"
                                                                 alt="" width="120" height="140" id="featuredimage" />  
                                                            <input type="hidden" id="hiddenfield" value="<?php  if(isset($r->photo)){ echo $r->photo;} ?>" name="photo" />
                                                        </button>
                                                        <!-- Button trigger modal -->
                                                    </td> 
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <h4>هوية اللاعب</h4>
                                <div class="row form-row">
                                  <div class="col-md-12">
                                    <input name="address" id="form3Address" type="text"  class="form-control" placeholder="العنوان" value="<?php if(isset($r->address)){ echo $r->address; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-6">
                                    <input name="city" id="form3City" type="text"  class="form-control" placeholder="المدينة" value="<?php if(isset($r->city)){ echo $r->city; }?>">
                                  </div>
                                  <div class="col-md-6">
                                    <input name="state" id="form3State" type="text"  class="form-control" placeholder="المحافظة /الولاية" value="<?php if(isset($r->state)){ echo $r->state; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-12">
                                    <input name="country" id="form3Country" type="text"  class="form-control" placeholder="الدولة" value="<?php if(isset($r->country)){ echo $r->country; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-6">
                                    <input name="contact_number" id="form3TeleNo" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="رقم التليفون" value="<?php if(isset($r->contact_number)){ echo $r->contact_number; }?>">
                                  </div>
                                  <div class="col-md-6">
                                    <input name="contact_number2" id="form3TeleNo" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="رقم التليفون" value="<?php if(isset($r->contact_number2)){ echo $r->contact_number2; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-6">
                                    <input name="contact_number3" id="form3TeleNo" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="رقم التليفون" value="<?php if(isset($r->contact_number3)){ echo $r->contact_number3; }?>">
                                  </div>
                                  <div class="col-md-6">
                                    <input name="contact_number4" id="form3TeleNo" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="رقم التليفون" value="<?php if(isset($r->contact_number4)){ echo $r->contact_number4; }?>">
                                  </div>
                                </div>
                                <div class="row small-text"></div>
                            </div>
                        </div>
                            
            </div>
          </div>
        </div>
      </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="tab5FollowUs">
                          <div class="row">
                            <div class="col-md-5">
                                <h4>البيانات الرياضية </h4>
                                <div class="row form-row">
                                  <div class="col-md-12">
                                    <input name="club" id="form3Address" type="text"  class="form-control" placeholder="عضو لدى نادى" value="<?php if(isset($r->club)){ echo $r->club; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-12">
                                    <input name="team" id="form3City" type="text"  class="form-control" placeholder="يلعب لمنتخب" value="<?php if(isset($r->team)){ echo $r->team; }?>">
                                  </div>
                                  <div class="col-md-8">
                                    <input name="participate" id="form3State" type="text"  class="form-control" placeholder="البطولة المشارك بها" value="<?php if(isset($r->participate)){ echo $r->participate; }?>">
                                  </div>
                                  <div class="col-md-4">
                                        <input name="champions" id="form3Country" type="text"  class="form-control" placeholder="عدد مرات المشاركة" value="<?php if(isset($r->champions)){ echo $r->champions; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                  <div class="col-md-6">
                                    <input name="rank" id="form3PostalCode" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="الرقم التصنيفى العالمى" value="<?php if(isset($r->rank)){ echo $r->rank; }?>">
                                  </div>
                                  <div class="col-md-6">
                                    <input name="localrank" id="form3PostalCode" type="text"  class="form-control" style="font-family: 'Open Sans' !important;" placeholder="الرقم التصنيفى المحلى" value="<?php if(isset($r->localrank)){ echo $r->localrank; }?>">
                                  </div>
                                </div>
                                <div class="row small-text"></div>
                            </div>
                              
                            <div class="col-md-7">
                                <h4>البطولات المشارك بها اللاعب  </h4>
                                <div class="row form-row champion-table">
                                    <table class="table table-bordered"> 
                                        <?php if(isset($player_champs)){ ?> 
                                        <tr>
                                            <th> البطولات التى شارك بها اللاعب</th>
                                            <th> عدد مرات المشاركة</th>
                                            <th> حذف</th>
                                        </tr>
                                        <?php foreach ($player_champs as $pl){ ?> 
                                        <tr>
                                            <td style="vertical-align: middle;"> <?php echo $pl->champ_name; ?> </td>
                                            <td style="font-family:'Open Sans'!important;vertical-align: middle;"> <?php echo $pl->champ_count; ?> </td>
                                            <td> <button class="btn btn-danger btn-remove-champ" type="button" data-id="<?php echo $pl->id; ?>" style="padding:5px 15px;"> <i class="fa fa-minus"></i></button> </td>
                                        </tr>
                                        <?php }} ?>
                                    </table>
                                    <div class="row form-row">
                                        <div class="col-md-8">
                                            <select class="form-control" name="champ[]" id="championshipfield">
                                                <option value=""> -- choose championship -- </option>
                                                <?php if(count($champs)){ foreach ($champs as $champ){ ?> 
                                                <option value="<?php echo $champ->champ_id; ?>"> <?php echo $champ->champ_name ; ?> </option>
                                                <?php } ?> 
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input name="champcount[]" id="form3Country" type="text"  class="form-control" placeholder="عدد مرات المشاركة" value="<?php if(isset($r->champions)){ echo $r->champions; }?>">
                                            <input type="hidden" name="champ_with_id[]" value="<?php if(isset($champ->champ_id)){ echo $champ->champ_id;} ?>" />
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                                
                                <div class="row form-row">
                                    <div class="col-md-2">
                                        <button class="btn btn-danger" type="button" id="add_champ">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="row small-text"></div>
                            </div>
                          </div>
                        </div>
                          
                          
                        <div class="tab-pane" id="tab5photos">
                            <h4>معرض الصور </h4>
                            <button class="btn btn-danger add_btn pull-left" style="margin-bottom: 15px;"> <i class="fa fa-plus"></i> إضافة صورة جديدة </button>
                            <div class="clearfix"></div>
                                    <?php if(isset($images)){ foreach ($images as $image){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-append clearfix">
                                       <table class="table table-bordered images-table">
                                           <tbody>
                                            <input id="fieldID<?php if(isset($image->image_id)){ echo $image->image_id;} ?>" type="hidden" value="">
                                              <tr class="image-data"> 
                                                <td width="20%"> 
                                                    <img src="<?php if(isset($image->image_name)){ echo base_url().'source/'.$image->image_name;}else{ echo base_url().'images/noimage.png';}?>" class="newpic" alt="" width="100" height="100" border="1"/> 
                                                    <input type="hidden" class="pic" name="uploaded_photo[]" value="<?php if(isset($image->image_name)){ echo $image->image_name;}?>" />
                                                    <a data-toggle="modal" data-target="#myModal" class="btn launch" type="button">Select</a> 
                                                </td>
                                                <td> <input class="fieldname form-control" type="text" value="<?php if(isset($image->image_name)){ echo $image->image_name;}?>" ></td>
                                                <td> <input type="hidden" name="uploaded_photo_id[]" value="<?php if(isset($image->image_id)){ echo $image->image_id;} ?>" /></td>
                                                <td> <button class="btn btn-danger btn-del" data-id="<?php if(isset($image->image_id)){ echo $image->image_id;} ?>"><i class="fa fa-trash"></i> </button> </td>
                                              </tr>  
                                           </tbody>
                                       </table>
                                    </div>
                                </div> 
                            </div>
                                    <?php }} ?>
                            
                        </div>
                      </div>
                      
                    </div>
                    
                                    <div class="pull-right">
                                        <button class="btn btn-danger btn-cons" type="submit" ><i class="icon-ok"></i>
                                          <?php if($this->uri->segment(3) == TRUE || isset($rows)) { echo 'تحديث';} else { echo 'حفظ';} ?> 
                                        </button>
                                      <button class="btn btn-white btn-cons" type="button">إلغاء</button>
                                    </div>
                              
			<?php echo form_close(); ?>
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
    

