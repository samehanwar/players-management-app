    <div class="modal fade" id="myModal" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">image manager</h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content" >  
            <ul class="breadcrumb">
                <li>
                  <p>انت الأن</p>
                </li>
                <li><a href="#" class="active">اضافة بطولة جديدة </a> </li>
            </ul>
            <div class="clearfix"></div>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <div class="page-title" style="margin-bottom: 30px;display: inline-block; margin-top: 10px;"> 
                <a href="<?php echo base_url(); ?>championships/add_championship"><button class="btn btn-success " style="padding: 5px 35px;"> <i class="fa fa-plus" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> إضافة بطزلة جديدة  </button> </a>
            </div>
            <div class="page-title" style="margin-bottom: 30px;display: inline-block; margin-right: 20px; margin-top: 10px;"> 
                <a href="<?php echo base_url(); ?>championships/list_championships"><button class="btn btn-success " style="padding: 5px 35px;"> <i class="fa fa-plus" style="color: #fff;font-size: 13px;display: inline-block;margin-top: -4px;"></i> العودة لقائمة البطولات</button> </a>
            </div>
        </div>
            <?php if(isset($status) && !empty($status)){ echo $status; } ?>
            <div class="page-title">  </div>	
            <?php if(isset($rows)) { foreach ($rows as $r){ } }?>
            <?php if(!isset($rows)){ echo form_open_multipart('championships/create_championship');} else{echo form_open_multipart('championships/edit_championship/'.$r->champ_id);} ?>
                <div class="row" >
                    <div class="col-md-12">
                      <ul class="nav nav-tabs" id="tab-5">
                        <li class="active pull-right" style="float: right !important;"><a href="#tab5hellowWorld" data-toggle="tab" style="padding: 15px 35px;font-size: 19px;">بيانات البطولة </a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab5hellowWorld">
                          <div class="row column-seperation">
                            <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    <form class="form-no-horizontal-spacing" id="form-condensed">	
                        <div class="row column-seperation">
                            <div class="col-md-8">
                                <h4>بيانات البطولة</h4>            
                                <div class="row form-row">
                                  <div class="col-md-12">
                                      <input name="champ_name" id="form3FirstName" type="text"  class="form-control" placeholder="اسم البطولة" value="<?php if(isset($r->champ_name)){ echo $r->champ_name; }?>">
                                  </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                      <input name="champ_country" id="form3FirstName" type="text"  class="form-control" placeholder="الدولة الراعية للبطولة" value="<?php if(isset($r->champ_country)){ echo $r->champ_country; }?>">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                      <input name="champ_organiser" id="form3FirstName" type="text"  class="form-control" placeholder="الجهة المنظمة للبطولة" value="<?php if(isset($r->champ_organiser)){ echo $r->champ_organiser; }?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <div  style="margin-right: 40px;">
                                    <h4>شعار الابطولة</h4>
                                    <table>
                                        <tbody>
                                            <input type="hidden" id="fieldup" value="" />
                                                <tr class="meta-bordered image-data"> 
                                                    <td> 
                                                        <button type="button"  class="btn btn-default btn-lg image_btn" data-toggle="modal" data-target="#myModal">
                                                            <img src="<?php if(isset($r->photo) && strlen($r->photo) > 0 ){ echo base_url().'source/'.$r->photo;}  else {echo base_url().'images/gravatar.png';} ?>" class="img-responsive"
                                                                 alt="" width="120" height="140" id="featuredimage" />  
                                                            <input type="hidden" id="hiddenfield" value="<?php  if(isset($r->photo)){ echo $r->photo;} ?>" name="photo" />
                                                        </button>
                                                        <!-- Button trigger modal -->
                                                    </td> 
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                        </div>
                            
            </div>
          </div>
        </div>
      </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    
                                    <div class="pull-right">
                                        <button class="btn btn-danger btn-cons" type="submit" ><i class="icon-ok"></i>
                                          <?php if($this->uri->segment(3) == TRUE || isset($rows)) { echo 'تحديث';} else { echo 'حفظ';} ?> 
                                        </button>
                                      <button class="btn btn-white btn-cons" type="button">إلغاء</button>
                                    </div>
                              
			<?php echo form_close(); ?>
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
    

