


    <!-- *************************************************************  begin of page contents ********************************************************** -->
    <div class="page-content"> 
        <div class="content">  
            <div class="page-title"> <h3> messages lists </h3>  
                <a href="<?php echo base_url(); ?>message/add_message">  </a></div>		
                <div class="row dashboard">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> list all messages </h4>
                                
                                </div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">
                                                    <div class="tab-pane active" id="home">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <tr> <th> SN </th> <th> message text </th> <th> sender name </th> <th> sender mail </th> <th> message time </th>  <th> edit </th> <th> delete </th></tr>
                                                                <?php foreach ($rows as $r) { ?>
                                                                <tr class="meta-bordered"> 
                                                                    <td> <?php echo $r->msg_id; ?> </td> 
                                                                    <td> <?php echo $r->msg_txt; ?> </td>
                                                                    <td> <?php echo $r->msg_fname . " " . $r->msg_lname ; ?> </td>
                                                                    <td> <?php echo $r->msg_mail; ?> </td>
                                                                    <td> <?php echo $r->time_created; ?> </td>
                                                                    <td class="meta-action"><a href="<?php echo base_url(); ?>message/show_message/<?php echo $r->msg_id; ?>"> <button class="btn btn-info"> <i class="fa fa-edit"> </i> show </button> </a></td>
                                                                    <td class="meta-action"><a href="<?php echo base_url(); ?>message/delete_message/<?php echo $r->msg_id; ?>"> <button class="btn btn-info"> <i class="fa fa-close"></i> delete </button> </a></td>
                                                                </tr>
                                                                <?php } ?>
                                                            </table>
                                                        </div> <!-- end of table responsive -->
                                                    </div>
                            <div class="panel-footer">
                                <p> pagination goes here </p>
                            </div>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
