    <div class="modal fade" id="myModal" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">image manager</h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="page-content"> 
        <div class="content">  
            <div class="page-title"> <h3> add post  </h3> </div>	
            <?php if(isset($rows)) { foreach ($rows as $r){ } }?>
            <?php if(!isset($rows)){ echo form_open_multipart('post/create_post');} else{echo form_open_multipart('post/edit_post/'.$r->post_id);} ?>
                <div class="row dashboard">
                    <div class="col-md-12">
                        <div style="background:#fff;padding: 10px;" class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> post items </h4></div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">
                                <div class="tab-panel">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#general" data-toggle="tab"> general </a></li>
                                        <li><a href="#data" data-toggle="tab"> data </a></li>
                                        <li><a href="#images" data-toggle="tab">images </a></li>
                                    </ul> 
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="general">
                                            <div class="tabable">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#en" data-toggle="tab"> english </a></li>
                                                    <li><a href="#ar" data-toggle="tab"> arabic </a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="en">
                                                        <div class="table-responsive">
                                                            <table class="table ">
                                                                
                                                                <tr>
                                                                    <td> post title </td>
                                                                    <td> <input type="text" name="post_title" value="<?php if(isset($r->post_title)){ echo $r->post_title;} ?>" />  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> post description </td>
                                                                    <td> <textarea  name="post_desc" id="summernote"> 
                                                                            <?php if(isset($r->post_desc)){ echo $r->post_desc;} ?>
                                                                        </textarea> </td>
                                                                </tr>
                                                            </table> <!-- end of table -->
                                                        </div> <!-- end of responsive -->
                                                    </div> <!-- end oftab-pane -->
                                                    <div class="tab-pane" id="ar">
                                                        <div class="table-responsive">
                                                            <table class="table ">
                                                                <tr>
                                                                    <td> عنوان المحتوى  </td>
                                                                    <td> <input type="text" name="post_title_ar" value="<?php if(isset($r->post_title_ar)){ echo $r->post_title_ar;} ?>" />  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> تفاصيل المحتوى  </td>
                                                                    <td> <textarea  name="post_desc_ar"  id="summernote"> 
                                                                              <?php if(isset($r->post_desc_ar)){ echo $r->post_desc_ar;} ?>
                                                                        </textarea> </td>
                                                                </tr>
                                                            </table> <!-- end of table -->
                                                        </div> <!-- end of responsive -->
                                                    </div> <!-- end oftab-pane -->
                                                </div> <!-- end of tab-content -->
                                            </div> <!-- end of tabable -->
                                        </div> <!-- end of tab-pane -->
                                        <div class="tab-pane" id="data">
                                            <div class="table-responsive">
                                                <table class="table ">
                                                    <tr> <td> </td></tr>
                                                    <input type="hidden" id="fieldup" value="" />
                                                    <tr class="meta-bordered image-data"> 
                                                        <td> featured image  </td> 
                                                        <td colspan="3"> 
                                                            <button type="button"  class="btn btn-primary btn-lg image_btn" data-toggle="modal" data-target="#myModal">
                                                                <img src="<?php if(isset($r->post_photo)){ echo base_url().'source/'.$r->post_photo;}  else {echo base_url().'images/noimage.png';} ?>"
                                                                         alt="" width="150" height="150" id="featuredimage"/>  
                                                                <input type="hidden" id="hiddenfield" value="<?php  if(isset($r->post_photo)){ echo $r->post_photo;} ?>" name="post_photo" />
                                                            </button>
                                                            <!-- Button trigger modal -->
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td> page target </td> 
                                                        <td> 
                                                            <select class="form-control" name="post_target">
                                                                <option value="<?php if(isset($r->page_id)){ echo $r->page_id; }  ?>" >
                                                                    <?php if(isset($r->page_id)){ echo $r->page_name; } ?> </option>
                                                                <option> ------------------ </option>
                                                                <?php 
                                                                 foreach ($page as $p){
                                                                     echo '<option value="'.$p->page_id.'" >'.$p->page_name.'</option>';
                                                                }
                                                                ?>
                                                            </select> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> category target </td> 
                                                        <td> 
                                                            <select class="form-control" name="post_cat">
                                                                <option value="<?php if(isset($r->post_cat)){ echo $r->post_cat; }  ?>" >
                                                                    <?php 
                                                                    if(isset($r->post_cat)){
                                                                        foreach ($cat as $c){ 
                                                                            if($c->sec_id == $r->post_cat){
                                                                                echo $c->sec_name;
                                                                            }
                                                                        } 
                                                                    }
                                                                    ?>
                                                                </option>
                                                                <option> ------------------ </option>
                                                                <?php 
                                                                 foreach ($cat as $c){ 
                                                                    echo '<option value="'.$c->sec_id.'" >'.$c->sec_name.'</option>';
                                                                }
                                                                ?>
                                                            </select> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> item order </td>
                                                        <td> <input type="text" class="form_control" value="<?php  if(isset($r->post_order)){ echo $r->post_order;} ?>" name="post_order" /> </td>
                                                    </tr>
                                                </table>
                                            </div> <!-- end of table responsive -->
                                        </div> <!-- end of tab-pane -->
                                        <div class="tab-pane" id="images">
                                            <button class="btn btn-danger add_btn pull-right" style="margin-bottom: 15px;"> <i class="fa fa-plus"></i> add image field </button>
                                            <?php if(isset($images)){ foreach ($images as $image){ ?>
                                            <div class="input-append clearfix">
                                               <table class="table table-bordered">
                                                <tbody>
                                                <input id="fieldID<?php if(isset($image->image_id)){ echo $image->image_id;} ?>" type="hidden" value="">
                                                <tr class="image-data"> 
                                                    <td> 
                                                        <img src="<?php if(isset($image->image_name)){ echo base_url().'source/'.$image->image_name;}else{ echo base_url().'images/noimage.png';}?>" class="newpic" alt="" width="100" height="100" border="1"/> 
                                                        <input type="hidden" class="pic" name="uploaded_photo[]" value="<?php if(isset($image->image_name)){ echo $image->image_name;}?>" />
                                                        <a data-toggle="modal" data-target="#myModal" class="btn launch" type="button">Select</a> 
                                                    </td>
                                                    <td> <input class="fieldname form-control" type="text" value="<?php if(isset($image->image_name)){ echo $image->image_name;}?>" ></td>
                                                    <td> <input type="hidden" name="uploaded_photo_id[]" value="<?php if(isset($image->image_id)){ echo $image->image_id;} ?>" /></td>
                                                    <td> <button class="btn btn-danger btn-del" data-id="<?php if(isset($image->image_id)){ echo $image->image_id;} ?>"><i class="fa fa-trash"></i> </button> </td>
                                                    
                                                </tr>
                                               </tbody>
                                               </table>
                                            </div>
                                            <?php }} ?>
                                        </div>
                                    </div> <!-- end of tab-content -->
                                </div>
                                
                                
                            </div> <!-- end of panel-body -->
                            <div class="panel-footer ">
                                <div class="" pull-right>
                                    <button type="submit" value="save" class="btn btn-success" name="sb"><i class="fa fa-save"></i>
                                        <?php if($this->uri->segment(3) == TRUE || isset($rows)) { echo 'update';} else { echo 'save';} ?> 
                                    </button>
                                    <a href="<?php echo base_url();?>post/list_post">
                                        <button type="button" value="cancel" class="btn btn-danger"><i class="fa fa-times"></i> cancel </button>
                                    </a>
                                    <button type="reset" value="reset" class="btn btn-primary"><i class="fa fa-eraser"></i> reset </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
    

