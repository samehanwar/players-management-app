


    <!-- *************************************************************  begin of page contents ********************************************************** -->
    <div class="page-content"> 
        <div class="clearfix"></div>
            <div class="content">  
            	<div class="row">
                    <div class="col-md-6">
                        <?php if(count($rows)){ foreach ($rows as $row){  }} ?>
				<div class=" tiles white col-md-12 no-padding">
                                    <div class="tiles green cover-pic-wrapper">						
                                        <div class="overlayer bottom-left">
                                            <div class="overlayer-wrapper">
                                                <!--<div class="padding-10 hidden-xs">									
                                                    <button type="button" class="btn btn-primary btn-small"><i class="fa fa-check"></i>&nbsp;&nbsp;Following</button> 
                                                    <button type="button" class="btn btn-primary btn-small">Add</button>
                                                </div>-->
                                            </div>
                                        </div>
                                        <img src="<?php echo base_url(); ?>assets/img/cover_pic.png" alt="">
                                    </div>
					<div class="tiles white">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3" >
                                                    <div class="" style="margin-right: 70px;">
                                                        <div class="user-profile-pic" style="width:100px;height:100px;">	
                                                            <img width="200" height="200" style="width:100px;height: 100px;margin-right: -20px;" data-src-retina="<?php echo base_url(); ?>source/<?php echo $row->photo; ?>" data-src="<?php echo base_url(); ?>source/<?php echo $row->photo; ?>" src="<?php echo base_url(); ?>source/<?php echo $row->photo; ?>" alt="">
                                                        </div>
                                                        <div class="user-mini-description">
                                                             <h3 class="text-success semi-bold" style="font-family: 'Open Sans' !important;">
                                                            <?php if(isset($row->rank)){ echo $row->rank;} ?>
                                                            </h3>
                                                            <h5>التصنيف الدولى </h5>
                                                            <h3 class="text-success semi-bold" style="font-family: 'Open Sans' !important;">
                                                            <?php if(!empty($row->localrank)){ echo $row->localrank;} else{ echo '<p style="font-size:13px;font-family: \'ge_flow_regular\' !important;">غير متاح الأن<p>'; } ?>
                                                            </h3>
                                                            <h5>التصنيف المحلى</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 user-description-box  col-sm-8">
                                                    <h4 class="semi-bold no-margin" style="margin-bottom:5px !important;"><?php if(isset($row->player_fname)){ echo $row->player_fname .'  '. $row->player_lname ;} ?></h4>
                                                    <h6 class="no-margin"><?php if(isset($row->occupation)){ echo $row->occupation .'  '. $row->occupation ;} ?></h6>
                                                    <br>
                                                    <p><i class="fa fa-briefcase"></i><?php if(isset($row->country)){ echo $row->country;} ?></p>
                                                    <p style="font-family: 'Open Sans' !important;"><i class="fa fa-globe"></i><?php if(isset($row->dateofbirth)){ echo $row->dateofbirth;} ?></p>
                                                    <p style="font-family: 'Open Sans' !important;"><i class="fa fa-file-o"></i><?php if(isset($row->contact_number)){ echo $row->contact_number;} ?> - <?php if(isset($row->contact_number2)){ echo $row->contact_number2;} ?> - <?php if(isset($row->contact_number3)){ echo $row->contact_number3;} ?></p>
                                                    <p style="font-family: 'Open Sans' !important;"><i class="fa fa-envelope"></i><?php if(isset($row->email)){ echo $row->email;} ?></p>
                                                </div>	
                                            </div>
				    <div class="tiles-body">
                                      <div class="tab-profile-player">
                                          <ul class="nav nav-tabs" style="background: #f2f4f6;">
                                              <li class="active"><a href="#champs" data-toggle="tab">البطولات المشارك بها</a></li>
                                              <li><a href="#photos" data-toggle="tab"> الصور</a></li>
                                          </ul>
                                          <div class="tab-content"> 
                                              <div class="tab-pane active" id="champs">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th> SN. </th>
                                                                <th> championship name </th>
                                                                <th> participation times </th>
                                                            </tr>
                                                            <?php $n = 1; if(count($player_champs)) { foreach ($player_champs as $pl) { ?> 
                                                            <tr>
                                                                <td> <?php echo $n++; ?> </td>
                                                                <td> <?php echo $pl->champ_name; ?> </td>
                                                                <td> <?php echo $pl->champ_count; ?> </td>
                                                            </tr>
                                                            <?php }} ?>
                                                        </table>
                                                    </div>
                                              </div>
                                              <div class="tab-pane" id="photos">
                                                  <?php if(count($images)){foreach ($images as $image){ ?> 
                                                  <div class="photo-gallery">
                                                      <div class="col-md-3">
                                                          <img src="<?php echo base_url(); ?>source/<?php echo $image->image_name; ?>" width="100%" height="120" alt=""   />
                                                      </div>
                                                  </div>
                                                  <?php }}?>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                            </div>
                        </div>	
                    </div>
		</div>
                        
                        

         
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
