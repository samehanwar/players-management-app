

    <div class="page-content"> 
        <div class="content">  
            <div class="page-title"> <h3> add section  </h3> 
                <?php if($this->uri->segment(3) == TRUE || isset($rows)) {  ?> 
            <a href="<?php echo base_url(); ?>sections/add_section"> <button class="btn btn-success pull-right"> <span class="glyphicon glyphicon-user"> </span> create new section </button> </a>
                <?php } ?>
            </div>		
                <div class="row dashboard">
                    <div class="col-md-12">
                        <div style="background:#fff;padding: 10px;" class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> section details </h4></div>   
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">

                                                        <div class="table-responsive">
                                                            <table class="table ">
                                                                <?php if(isset($rows)) { foreach ($rows as $r){ } }?>
                                                                <?php if(!isset($rows)){ echo form_open_multipart('sections/create_section');} else{echo form_open_multipart('sections/edit_section/'.$r->sec_id);} ?>
                                 
                                                                <tr>
                                                                    <td> section name </td>
                                                                    <td> <input type="text" name="sec_name" value="<?php if(isset($r->sec_name)){ echo $r->sec_name;} ?>" />  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> page target </td>
                                                                    <td> 
                                                                        <select name="sec_target">
                                                                            <option> <?php if(isset($r->sec_target)){ echo $r->sec_target; } else { echo '-- choose page target --'; ?> </option> 
                                                                            <?php } ?>
                                                                            <?php foreach ($pages as $page){ ?>
                                                                            <option> <?php echo $page->page_name; ?> </option>
                                                                            <?php } ?> 
                                                                        </select> 
                                                                    </td>
                                                                </tr>
                                                            </table> <!-- end of table -->
                                                        </div> <!-- end of responsive -->
         
                                
                            </div> <!-- end of panel-body -->
                            <div class="panel-footer ">
                                <div class="" pull-right>
                                    <button type="submit" value="save" class="btn btn-success"><i class="fa fa-save"></i> 
                                        <?php if($this->uri->segment(3) == TRUE || isset($rows)) { echo 'update';} else { echo 'save';} ?> 
                                    </button>
                                    <a href="<?php echo base_url();?>sections/list_sections"> 
                                        <button type="button" value="cancel" class="btn btn-danger"><i class="fa fa-times"></i> cancel </button>
                                    </a>
                                    <button type="reset" value="reset" class="btn btn-primary"><i class="fa fa-eraser"></i> reset </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
    

