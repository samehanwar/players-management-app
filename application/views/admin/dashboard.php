
    <!-- *************************************************************  begin of page contents ********************************************************** -->
    <div class="page-content"> 
        <div class="content">  
            <div class="page-title"> <h3> لوحة التحكم </h3> </div>		
                <div class="row dashboard">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> اللاعبين المضافين </h4></div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body" >
                                <button class="btn btn-primary" style="vertical-align:middle;font-family: 'Open Sans' !important; margin: 0px 10px;"> <?php if(count($players)){ echo $players; } ?> </button>
                                <span style="vertical-align:middle;font-family: 'ge_flow_regular' !important; font-weight: bold; "> لاعـــب</span>
                            </div> <!-- end of panel-body -->
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                    
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> pages </h4></div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">
        
                            </div> <!-- end of panel-body -->
                            <div class="panel-footer">
                                <p> pagination goes here </p>
                            </div>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                    
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> posts </h4></div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">
        
                            </div> <!-- end of panel-body -->
                            <div class="panel-footer">
                                <p> pagination goes here </p>
                            </div>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                    
                    <div class="col-md-4">
               
                    </div> <!-- end of col-md-12 -->
                    
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"> <h4> messages </h4></div>    
                            </div> <!-- end of panel-heading -->
                            <div class="panel-body">
        
                            </div> <!-- end of panel-body -->
                            <div class="panel-footer">
                                <p> pagination goes here </p>
                            </div>
                        </div> <!-- end of panel-default -->
                    </div> <!-- end of col-md-12 -->
                </div> <!-- end of row -->
        </div> <!-- end of content -->
    </div> <!-- end of page-content -->
