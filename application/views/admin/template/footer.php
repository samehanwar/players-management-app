
</div> <!-- end of page container -->
<!-- END CONTAINER --> 
<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="<?php echo base_url(); ?>backend/js/jquery-2.1.1.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>backend/assets/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>backend/assets/js/breakpoints.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>backend/assets/js/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>backend/assets/js/core.js" type="text/javascript"></script> 
<script type="text/javascript" src="<?php echo base_url() ; ?>backend/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: ".textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
    </script>
    

    
    
    
    <script type="text/javascript">
        
        function responsive_filemanager_callback(field_id){
	console.log(field_id);
	var url=jQuery('#'+field_id).val();
        var ref = window.location.hostname , pro = window.location.protocol ;
        var newref = pro+"//"+ref+"/source/" ;
        var filename = url.replace(newref,'');
        url = '<?php echo base_url(); ?>source/' + filename;
            $('#'+field_id).siblings('tr.image-data').find('.newpic').attr('src',url);
            $('#'+field_id).siblings('tr.image-data').find('#featuredimage').attr('src',url);
            $('#'+field_id).siblings('tr.image-data').find('#hiddenfield').attr('value',filename);
            $('#'+field_id).siblings('tr.image-data').find('.pic').attr('value',filename);
            $('#'+field_id).siblings('tr.image-data').find('.fieldname').attr('value',filename);
        }
        $.fn.hasAttr = function(name) {  
            return this.attr(name) !== undefined;
        };
    $(document).ready(function(){
        
//        $('#calcbowling').find('.frame-box input#frame1').on('change',function(){
//            var attempt1 = $(this).val();
//            var attempt2 = $(this).next('input').val();
//            console.log('thiis is a' + attempt1 +  attempt2 );
//        });

        $('.dashboard-bowling').find('input').on('change',function(e){
            var inputval  = $(this).val(), field = $(this),
                fieldname = $(this).attr('name'), 
                fieldnum = fieldname.slice(-1),
                prev = field.siblings().val(),
                sumof = parseInt(prev) + parseInt(inputval),
                framesum = field.parents('.score-fields').siblings('input'),
                prevframsum = field.parents('td').prev('td').find('input').last(),
                prevframefield = field.parents('td').prev('td').find('input').eq(1),
                prevframefield2 = field.parents('td').prev('td').find('input').eq(1).val(),
                prevframefield3 = field.parents('td').prev('td').prev('td').find('input').eq(1),
                prevframefield3val = field.parents('td').prev('td').prev('td').find('input').eq(1).val(),
                prevframsum3 = field.parents('td').prev('td').prev('td').find('input').last(),
                firstframe = $('td#first'),
                prevframeval = prevframsum.val(),
                sumoftwoframes = parseInt(sumof) + parseInt(prevframeval),
                totalscore = $('#totalscore');
                 
                if(prevframsum.val()== 'undefined'){ prevframsum.val() = 0 }
                var netsum = parseInt(sumof) + parseInt(prevframsum.val());
                
                if(fieldnum == 1) {
                    if(inputval == 10){
                        field.val('');
                        field.next().val('X');
                        if(field.parents('td').attr('id') == "first"){
                            framesum.val(10);
                            field.parents('td').next('td').find('input').first().focus();
                        }
                        else{
                            if(prevframefield2 == '/'){
                                prevframsum.val( parseInt(prevframsum.val()) + 10 );
                                framesum.val(10+parseInt(prevframsum.val()));
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val == 'X') {
                                prevframsum3.val(10 + parseInt(prevframsum3.val()));
                                prevframsum.val( 10 + 10 + parseInt(prevframsum3.val()));
                                framesum.val(10 + parseInt(prevframsum.val()));
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val !== 'X') {
                                prevframsum.val( parseInt(prevframsum.val()) + 10 );
                                framesum.val(10+parseInt(prevframsum.val()));
                            }
                            else if(prevframefield2 !== '/' || prevframefield2 !== 'X'){
                                prevframsum.val( parseInt(prevframsum.val()));
                                framesum.val(10+parseInt(prevframsum.val()));
                            }
                            field.parents('td').next('td').find('input').first().focus();
                            totalscore.text(prevframsum.val());
                        }
                    }
                }
                

                
                if(fieldnum == 2) {
                    
                    if( sumof  == 10 ) {
                        field.val('/');
                        if(field.parents('td').attr('id') == "first"){
                            framesum.val(sumof);
                        }
                        else {
                            if(prevframefield2 == '/'){
                                prevframsum.val( parseInt(prevframsum.val()) + parseInt(prev) );
                                framesum.val(parseInt(prevframsum.val()) + 10);
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val == 'X') {
                                prevframsum3.val( parseInt(prevframsum3.val()) + parseInt(prev) );
                                prevframsum.val( sumof + 10 + parseInt(prevframsum3.val()));
                                framesum.val(sumof + parseInt(prevframsum.val()));
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val !== 'X') {
                                prevframsum.val( parseInt(prevframsum.val()) + 10);
                                framesum.val(sumoftwoframes + 10);
                            }
                            else if(prevframefield2 !== '/' || prevframefield2 !== 'X'){
                                prevframsum.val( parseInt(prevframsum.val()));
                                framesum.val(sumoftwoframes);
                            }
                        }
                        totalscore.text(prevframsum.val());
                        field.parents('td').next('td').find('input').first().focus();
                        
                    }
                    else if( sumof  > 10 ) {
                        field.val('');
                        $('#myModal3').modal();
                        field.focus();
                    }
                    else if( sumof  < 10 ) {
                        if(field.parents('td').attr('id') == "first"){
                            framesum.val(sumof);
                        }
                        else{
                            if(prevframefield2 == '/'){
                            prevframsum.val( parseInt(prevframsum.val()) + parseInt(prev));
                            framesum.val(sumoftwoframes + parseInt(prev));
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val == 'X') {
                                prevframsum3.val( parseInt(prevframsum3.val()) + parseInt(prev) );
                                prevframsum.val( sumof + 10 + parseInt(prevframsum3.val()));
                                framesum.val(sumof + parseInt(prevframsum.val()));
                            }
                            else if(prevframefield2 == 'X' && prevframefield3val !== 'X') {
                                prevframsum.val( parseInt(prevframsum.val()) + sumof);
                                framesum.val(sumoftwoframes + sumof);
                            }
                            else if(prevframefield2 !== '/' || prevframefield2 !== 'X'){
                                prevframsum.val( parseInt(prevframsum.val()));
                                framesum.val(sumoftwoframes);
                            }
                        }
                    }
                    totalscore.text(prevframsum.val());
                    console.log(inputval + ' ' + prev + ' ' + sumof + ' ' + sumoftwoframes + prevframefield2 + prevframefield3val  );
                }
                    totalscore.text(framesum.val());
                console.log(inputval + ' ' + prev + ' ' + sumof + ' ' + sumoftwoframes + prevframefield2 + prevframefield3val + prevframsum3.val() + prevframsum.val()  );
                
                $.ajax({
                    url : '<?php echo base_url(); ?>bowling_calculate/check_valid_values',
                    data: {value : inputval , name : fieldname},
                    type: 'POST',
                    success:function(data){
                        if(data !== "1" && data !== "10"){
                            field.val('');
                            $('#myModal2').modal();
                        }
                    }
                });
        });
        
        
        
        
        
        
        
        $('#myModal2').on('show.bs.modal', function (event) {
            var modal = $(this);
            var recipient = '<p> <i class="fa fa-close fa-3x modal-fa"> </i> </p><h3 > you entered a wrong number <br/> please enter a valid number between 0 - 10  </h3>';
            modal.find('.modal-body').html(recipient);
        });
        
        $('#myModal3').on('show.bs.modal', function (event) {
            var modal = $(this);
            var recipient = '<p> <i class="fa fa-close fa-3x modal-fa"> </i> </p><h3 > wong value <br/> you exceed the limit sum of 10  </h3>';
            modal.find('.modal-body').html(recipient);
        });
        
        
          
        $('#frame-buttons').children('button').on('click',function(e){
            var valuenum = $(this).data('value');
            var currentele = $('#calcbowling').find('.frame-box input'),
                eleroll = currentele.data('roll'),
                eleval = currentele.val();
            
            console.log( valuenum + currentele + eleroll + eleval );
//            $('#calcbowling').find('.frame-box input').attr('value',valuenum).attr('disabled','disabled');
            var emptyfield = $('#calcbowling').find('.frame-box input');
//                $.ajax({
//                    url : '<?php echo base_url(); ?>bowling_calculate/calculate',
//                    data: {value : valuenum},
//                    type: 'POST',
//                    success:function(data){
//                        $('.dashboard-bowling').remove();
//                        $('#add-data').append(data);
//                    }
//                });
            
            
            console.log(valuenum);
        });

        $('.add_btn').on('click',function(e){
           e.preventDefault();
           var num = $('.input-append').length + 1;
           console.log(num);
           var render = '';
               render += '<div class="row imagefiled"><div class="col-md-12"><div class="input-append clearfix"> <table class="table table-bordered"><tbody>';
               render += '<input id="fieldID4'+num+'" type="hidden" value="" />';
               render += '<tr class="image-data">';
               render += '<td width="20%"><img src="<?php echo base_url(); ?>images/noimage.png" class="newpic" alt="" width="100" height="100" border="1"/>';
               render += '<input type="hidden" class="pic" name="uploaded_photo[]" value="" />';
               render += '<a data-toggle="modal" data-target="#myModal"  class="btn launch" type="button">Select</a></td>';
               render += '<td><input class="fieldname form-control" type="text" value="" /></td>';
               
               render += '<td><button type="button" class="btn btn-danger btn-del noid"><i class="fa fa-trash"></i> </button></td>';
               render += '</tr> </tbody></table> </div></div></div>';
            $('#tab5photos').append(render);   
        });
        
        $('#myModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) ;
            var ord = button.closest('tr.image-data').siblings('input').attr('id');
            var modal = $(this);
            var recipient = '<iframe width="100%" height="700" src="<?php echo base_url(); ?>filemanager/dialog.php?type=2&amp;field_id='+ord+'&amp;fldr="></iframe>';
            modal.find('.modal-body').html(recipient);
        });
        
        
        
        // add button for championship filed 
        $('#add_champ').on('click',function(e){
            e.preventDefault();
            var num = $('.champion-table').length + 1;
            var render = '';
               render += '<div class="row form-row champion-table"><div class="col-md-8">';
               render += '<select class="form-control droplistoption" name="champ[]"><option value="null"> -- choose championship -- </option> <?php if(count($champs)){ foreach ($champs as $champ){ echo '<option value="'.$champ->champ_id.'">'.$champ->champ_name.'</option>';}}?>';
               render += '';
               render += '</select></div>'; 
               render += '<div class="col-md-4">';
               render += '<input name="champcount[]" id="form3Country" type="text"  class="form-control" placeholder="عدد مرات المشاركة" value="">';
               render += '</div></div>';
            $('.champion-table').last().after(render);  
            console.log('this is abutton' + num);
        });
        
        
        $('.btn-remove-champ').on('click',function(e){
            e.preventDefault();
            var id = $(this).data('id'),
                current_url =  window.location.href ;
            $.ajax({
               url: '<?php echo base_url(); ?>players/delete_champ_field',
               data: {id : id},
               type: 'POST',
               success:function(data){
                   window.location = current_url;
            }
        });
            
            
            
            console.log('this is a test ' + id);
        });
        
        // 
        $('.droplistoption').on('select',function(e){
            e.preventDefault();
            console.log(e.target());
        });
        

                                  
                                
 
        $('#tab5photos').on('click','.btn-del',function(e){
            e.preventDefault();
            var imageId = $(this).data('id');
            console.log(imageId);
            var current_url =  window.location.href ;
            $.ajax({
               url: '<?php echo base_url(); ?>players/delete_gallery_image',
               data: { id : imageId},
               type:'POST',
               success:function(data){
                    window.location = current_url;
                    $('.tab-panel').find('a').attr('href','#images').parent('li').addClass('active');
                }
            });
            if(imageId == null || imageId == "undefined"){
                $(this).parents('.imagefiled').remove();
                $('.tab-panel').find('a').attr('href','#images').parent('li').addClass('active');
                console.log('that is a delete');
            }
        });
        
        
//       $('#myTab a').click(function(e) {
//            e.preventDefault();
//            $(this).tab('show');
//          });
//
//          // store the currently selected tab in the hash value
//          $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
//            var id = $(e.target).attr("href").substr(1);
//            window.location.hash = id;
//          });
//
//          // on load of the page: switch to the currently selected tab
//          var hash = window.location.hash;
//          $('#myTab a[href="' + hash + '"]').tab('show'); 
       
        
        $('.well-success').on('click',function(e){
           e.preventDefault();
           $(this).fadeOut();
        });
        
        // social media details 
        $('.social_choice').find('li').on('click',function(){
           $(this).parents('div.dropdown').append(this);
           console.log('that social media link');
        });

       $('.alert-flash').delay(5000).slideUp();
       $('.alert-flash').on('click',function(){
           $(this).hide();
       });
    });
    
</script>
<!-- END CORE TEMPLATE JS --> 
</body>
</html>