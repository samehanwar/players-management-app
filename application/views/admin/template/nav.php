

<div class="page-container row-fluid">
    <div class="page-sidebar" id="main-menu"> 
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
            <div class="user-info-wrapper">	
                <div class="profile-wrapper">
                    <img src="<?php echo base_url(); ?>backend/assets/img/profiles/avatar.jpg"  alt="" data-src="<?php echo base_url(); ?>backend/assets/img/profiles/avatar.jpg" data-src-retina="<?php echo base_url(); ?>assets/img/profiles/avatar2x.jpg" width="69" height="69" />
                </div>
                <div class="user-info">
                    <div class="greeting">مرحبا بك</div>
                    <div class="username"> <span class="semi-bold">admin</span></div>
                    <div class="status">الحالة<a href="#"><div class="status-icon green"></div>متصل</a></div>
                </div>
            </div> 
            <p class="menu-title">تصفح <span class="pull-right"><a href="javascript:;"><i class="fa fa-refresh"></i></a></span></p>
            <ul>	
                <li class="start active "> <a href="<?php echo base_url(); ?>dashboard"> <i class="icon-custom-home"></i> <span class="title">لوحة التحكم</span> 
                <li class=""> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">اللاعبين</span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                      <li > <a href="<?php echo base_url();?>players/list_players"> عرض كل اللاعبين </a> </li>
                    </ul>
                </li>
                <li > <a href="<?php echo base_url();?>federations/list_federations">  الإتحادات </a> </li>
                <li > <a href="<?php echo base_url();?>championships/list_championships">  البطولات </a> </li>
                <li class=""> <a href="javascript:;"> <i class="fa fa fa-adjust"></i> <span class="title">حساب نقاط البولينج</span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                      <li > <a href="<?php echo base_url();?>bowling_calculate/show_board"> حساب نقاط البولينج</a> </li>
                    </ul>
                </li>
                <li class=""> <a href="javascript:;"> <i class="fa fa fa-users"></i> <span class="title">المستخدمين</span> <span class="arrow "></span> </a>
                    <ul class="sub-menu">
                      <li > <a href="<?php echo base_url();?>admin/list_admin"> المستخدمين </a> </li>
                      <li > <a href="<?php echo base_url();?>admin/add_admin"> إضافة مستخدم جديد </a> </li>
                    </ul>
                </li>
            </ul>
        </div> <!-- end of page-sidebar-wrapper scrollbar-dynamic -->
    </div> <!-- end of page-sidebar -->
 
    <div class="footer-widget">		
	<div class="progress transparent progress-small no-radius no-margin">
            <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" ></div>		
	</div>
	<div class="pull-right">
            <div class="details-status"> </div>
            <a href="<?php echo base_url(); ?>admin/log_out"><i class="fa fa-power-off"></i></a>
        </div>
    </div>