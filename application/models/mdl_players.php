<?php
/**
 * Description of mdl_players
 *
 * @author Media R01
 */
class mdl_players extends MY_Model {
    
    protected $table_name = 'players';
    protected $id = 'player_id';
    
    public function get_filtered_data($filter,$searchtype,$num = NULL , $offset = NULL){
        if($filter && !$num && !$offset){
            $this->db->select('*');
            $this->db->from($this->table_name);
            $this->db->like($searchtype,$filter['proname']);
        }
        else {
            $this->db->select('*');
            $this->db->from($this->table_name);
            $this->db->like($searchtype,$filter['proname']);
            $this->db->limit($num,$offset);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            return $query->result();
        }
    }
    
    
    public function get_player_champions($id){
        $this->db->select('*');
        $this->db->where('player_id',$id);
        $this->db->from('players_championships');
        $this->db->join('championships','championships.champ_id = players_championships.champ_id');
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            return $query->result();
        }
    }
    
    public function get_images($id) {
        $this->db->where('proimg_id',$id);
        $query = $this->db->get('player_images');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    
    public function get_player_champs($id) {
        $this->db->where('player_id',$id);
        $query = $this->db->get('players_championships');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    public function remove_image($id,$table){
        $id = (int) $id;
        $this->db->where('image_id',$id);
        $this->db->delete($table);
    }
    
    public function remove_champ_field($id,$table) {
        $id = (int) $id;
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    
}
