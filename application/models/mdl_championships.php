<?php
/**
 * Description of mdl_players
 *
 * @author Media R01
 */
class mdl_championships extends MY_Model {
    
    protected $table_name = 'championships';
    protected $id = 'champ_id';
    
    public function get_filtered_data($filter,$searchtype,$num = NULL , $offset = NULL){
        if($filter && !$num && !$offset){
            $this->db->select('*');
            $this->db->from($this->table_name);
            $this->db->like($searchtype,$filter['proname']);
        }
        else {
            $this->db->select('*');
            $this->db->from($this->table_name);
            $this->db->like($searchtype,$filter['proname']);
            $this->db->limit($num,$offset);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            return $query->result();
        }
    }
    
    
    
    
}
