<?php

    class mdl_categories extends CI_Model {

        protected $table_name = 'category';
        protected $id = 'cat_id';

        
        public function get_all_categories($num = NULL , $offset = NULL) {
            if($num && $offset){
                $query = $this->db->get($this->table_name,$num,$offset);
            }
            elseif($num && !$offset){
                $query = $this->db->get($this->table_name,$num);
            }
            else {
                $query = $this->db->get($this->table_name);
            }
            if($query->num_rows() > 0 ) {
                return $query->result();
            }
        }
        
        public function get_all_categories_arr($num = NULL , $offset = NULL) {
            if($num && $offset){
                $query = $this->db->get($this->table_name,$num,$offset);
            }
            elseif($num && !$offset){
                $query = $this->db->get($this->table_name,$num);
            }
            else {
                $query = $this->db->get($this->table_name);
            }
            if($query->num_rows() > 0 ) {
                return $query->result_array();
            }
        }
        
        public function get_filtered_data($filter,$num = NULL , $offset = NULL){
            
            if($filter && !$num && !$offset){
                $this->db->select('cat_name,cat_name_ar,cat_id,time_created');
                $this->db->from($this->table_name);
                $this->db->like('cat_name',$filter);
                $this->db->or_like('cat_name_ar',$filter);
            }
            else {
                $this->db->select('cat_name,cat_name_ar,cat_id,time_created');
                $this->db->from($this->table_name);
                $this->db->like('cat_name',$filter);
                $this->db->or_like('cat_name_ar',$filter);
                $this->db->limit($num,$offset);
            }
            $query = $this->db->get();
            if($query->num_rows() > 0 ) {
                return $query->result();
            }
        }
        
        public function get_certain_category($id) {
            $this->db->where($this->id,$id);
            $query = $this->db->get($this->table_name);
            if($query->num_rows() > 0 ) {
                return $query->result();
            }
        }
        
        public function insert_category($data){
            $data_in = array(
                'cat_name' => $data['catname'],
                'cat_desc' => $data['catdesc'],
                'cat_name_ar' => $data['catnamear'],
                'cat_desc_ar' => $data['catdescar']
            );
            $this->db->insert($this->table_name,$data_in);
            return  $this->db->insert_id();
        }
        
        public function change_category($data){
            $data_up = array(
                'cat_name' => $data['catname'],
                'cat_desc' => $data['catdesc'],
                'cat_name_ar' => $data['catnamear'],
                'cat_desc_ar' => $data['catdescar']
            );
            $id = $data['id'];
            $this->db->where($this->id,$id);
            $this->db->update($this->table_name,$data_up);
            return  $this->db->insert_id();
        }
        
        public function remove_category($id){
            $this->db->where($this->id,$id);
            $this->db->delete($this->table_name);
            return TRUE;
        }
    }