<?php

    class mdl_post extends MY_Model {
        
        protected $table_name = 'posts';
        protected $id = 'post_id';
        
        
        
        
        public function get_post($id) {
            $this->db->where('post_id',$id);
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->join('pages','posts.post_target = pages.page_id');
            $query = $this->db->get();
            if($query->num_rows() > 0 ){
                return $query->result();
            }
        }
        
        public function get_post_data($id) {
            $this->db->where('post_target',$id);
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->join('pages','posts.post_target = pages.page_id');
            $this->db->join('sections','posts.post_cat = sections.sec_id');
            $query = $this->db->get();
            if($query->num_rows() > 0 ){
                return $query->result();
            }
        }
        
        public function get_limit($id) {
            $this->db->limit($id);
            $query = $this->db->get($this->table_name);
            if($query->num_rows() > 0 ){
                return $query->result();
            }
        }

        public function get_filtered_data($filter,$num = NULL , $offset = NULL){
            
            if($filter && !$num && !$offset){
                $this->db->select('post_title,post_title_ar,post_id,time_created,post_target,post_cat');
                $this->db->from($this->table_name);
                $this->db->like('post_title',$filter['proname']);
                $this->db->or_like('post_title_ar',$filter['proname']);
            }
            else {
                $this->db->select('post_title,post_title_ar,post_id,time_created,post_target,post_cat');
                $this->db->from($this->table_name);
                $this->db->like('post_title',$filter['proname']);
                $this->db->or_like('post_title_ar',$filter['proname']);
                $this->db->limit($num,$offset);
            }
            $query = $this->db->get();
            if($query->num_rows() > 0 ) {
                return $query->result();
            }
        }
        
        
        public function c_views($num){
            
            $this->db->select('views');
            $this->db->from($this->table_name);
            $this->db->where($this->id,$num);
            $row = $this->db->get()->row();
            
            $val = array('counts' => $row->counts + 1);
            $this->db->where($this->id,$num);
            $this->db->update($this->table_name,$val);
        }
        
        public function get_images($id) {
            $this->db->where('proimg_id',$id);
            $this->db->select('*');
            $this->db->from('post_images');
            $query = $this->db->get();
            if($query->num_rows() > 0 ) {
                return $query->result();
            }
        }
        
        public function count_post() {
                $query = $this->db->get('posts');
                return $query->num_rows();
        }
        public function get_page_id($slug) {
            $this->db->select('page_id')->from('pages')->where('page_name',$slug);
            $query = $this->db->get();
            foreach( $query->result() as $row ){ $id = $row->page_id; };
            return $id;
        }
        
        public function get_page_posts($slug) {
            $this->db->select('page_id')->from('pages')->where('page_name',$slug);
            $query = $this->db->get();
            foreach( $query->result() as $row ){ $id = $row->page_id; };
            echo $id;
            $res = $this->get_post_data($id);
            return $res;
//            var_dump($res);
        }
   
        

    } // end of clas  mdl _post 