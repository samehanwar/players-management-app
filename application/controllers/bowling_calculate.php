<?php

    class bowling_calculate extends CI_Controller{
    
        function __construct() {
            parent::__construct();
            $this->load->model('mdl_bowling','bowlingcalculate');
            $this->load->model('mdl_championships','championships');
        }
        
        public function show_board() {
            $data['champs']   = $this->championships->get();
            $data['content'] = 'admin/show_board';
            $this->load->view('admin/template/content',$data);
        }
        
        public function calculate(){
            $value = $this->input->post();
            var_dump($value);
            
            for($x=1;$x <= 10; $x++){
                $val1 = $this->check_for_value($value['F'.$x.'1'],1,$value['frame'.$x]);
                $type = $this->look_for_strike($val1);
                if($type == 'strike') {
                    $data[$value['frame'.$x]] = '10';
                    $data['F'.$x.'2'] = 'X';
                    $this->complete_frame($value['frame'.$x]);
                }
                else {
                    $val2 = $this->check_for_value($value['F'.$x.'2'],2,$value['frame'.$x]);
                    $type = $this->check_type($val1,$val2);
                    if($type == 'openframe') {
                        $data[$value['frame'.$x]] = $this->calculate_sum($value['F'.$x.'1'] , $value['F'.$x.'2']);
                        $data['F'.$x.'1'] = $value['F'.$x.'1'];
                        $data['F'.$x.'2'] = $value['F'.$x.'2'];
                        $this->complete_frame($value['frame'.$x]);
                    }
                    elseif($type == 'spare') {
                      $data[$value['frame'.$x]] = $this->calculate_sum($value['F'.$x.'1'] , $value['F'.$x.'2']);
                        $sumoftframe = $this->calculate_sum($value['F'.$x.'1'] , $value['F'.$x.'2']);
                        $data['F'.$x.'1'] = $value['F11'];
                        $data['F'.$x.'2'] = '/';
                        $this->complete_frame($value['frame'.$x]);
                    }
                }
            }
            $data['content'] = 'admin/show_board';
            $this->load->view('admin/template/content',$data);
        }
        
        
        public function check_for_value($value,$ord,$frame){
            is_null($value) ? $data[$frame] = NULL : $data[$frame] = $value;
            return $data[$frame];
        }

        private function calculate_sum($field1,$field2) {
            $sumof = $field1 + $field2;
            $result = $this->check_type($field1, $field2);
            return $sumof ;
        }
        
        private function look_for_strike($field1) {
            if($field1 == 10){
                $type = 'strike';
            }
            elseif( $field1 < 10 ){
                $type = 'notstrike';
            }
            return $type ;
        }

        
        private function check_type($field1,$field2) {
            if($field1 == 10){
                $type = 'strike';
            }
            else {
                if( $field1 + $field2 < 10 ) {
                     $type = 'openframe';
                }
                elseif( $field1 + $field2 == 10 ){
                     $type = 'spare';
                }
                elseif( $field1 + $field2 > 10){
                    return $type = 'out';
                }
            }
            return $type ;
        }
        
        private function complete_frame($frame){
            if($frame != 'F1'){
                return $frame;
            }
        }
        
        
        public function check_valid_values(){
           $ValidVal  = $this->input->post('value');
           $fieldname = $this->input->post('name');
           $fieldnum = substr($fieldname, -1) ;
           $pattern = '/^(10|[0-9]{1})$/';
           $res = preg_match($pattern, $ValidVal);
           if($res == TRUE){
               echo '1';
           }
           
           else {
               echo '0';
           }
        }
        
    }