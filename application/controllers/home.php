<?php
    class home extends CI_Controller {

        public function __construct() {
            parent::__construct();
            $lang=$this->session->userdata('lang')==null?'english':$this->session->userdata('lang');
            //$lang='english';
            $this->lang->load($lang,$lang);
            $this->load->model('mdl_categories');
            $this->load->model('mdl_manufacture');
            $this->load->model('mdl_marketing');
            $this->load->model('mdl_post');
            $this->load->model('mdl_sections');
            $this->load->model('mdl_page');
            $this->load->model('mdl_prices');
            $this->load->model('mdl_homepage');
            $this->load->library('pagination');
            $this->load->model('mdl_message','messages');
            $this->load->library('cart');
        }
        
        public function index() {
            $data['posts'] = $this->mdl_post->get_all();
            $data['postsl'] = $this->mdl_post->get_limit(2);
            $data['content'] = 'front/home';
            $this->load->view('front/template/content',$data);
        } 

        public function heading($en,$ar){
            if($this->session->userdata('lang') == 'english' || $this->session->userdata('lang') == ""){ echo $en ; }
            else { echo $ar ;}
        }

        public function contact(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['pages'] = $this->mdl_page->get_page_data();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/contact';
            $this->load->view('front/template/content',$data);
        }
        
        public function about(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/about';
            $this->load->view('front/template/content',$data);
        }
        
        public function prices(){
            $slug = $this->uri->segment(2);
            $data['prices'] = $this->mdl_prices->get();
            $data['rows'] = $this->mdl_post->get();
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/prices';
            $this->load->view('front/template/content',$data);
        }
        
        public function news(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/news';
            $this->load->view('front/template/content',$data);
        }
        
        public function news_details(){
            $id = $this->uri->segment(3);
            $data['posts'] = $this->mdl_post->get($id);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/news_details';
            $this->load->view('front/template/content',$data);
        }
        
         public function services(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/services';
            $this->load->view('front/template/content',$data);
        }
        
        public function media(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['sects'] = $this->mdl_sections->get();
            $data['content'] = 'front/media';
            $this->load->view('front/template/content',$data);
        }
        
        public function champion(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            foreach ($data['posts'] as $post){  $id = $post->post_id;};
            $data['images'] = $this->mdl_post->get_images($id); 
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/championships';
            $this->load->view('front/template/content',$data);
        }
        
        public function kids_area(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/kids-area';
            $this->load->view('front/template/content',$data);
        }
        
        public function play_bowling(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/play-bowling';
            $this->load->view('front/template/content',$data);
        }
        
        public function work(){
            $slug = $this->uri->segment(2);
            $data['posts'] = $this->mdl_post->get_page_posts($slug);
            $data['sects'] = $this->mdl_sections->get_all();
            $data['rows'] = $this->mdl_post->get();
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/works';
            $this->load->view('front/template/content',$data);
        }
        
         public function service_details(){ 
            $id = $this->uri->segment(3);
            $data['posts'] = $this->mdl_post->get($id);
            $data['rows'] = $this->mdl_post->get();
            $data['images'] = $this->mdl_post->get_images($id); 
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/service_details';
            $this->load->view('front/template/content',$data);
        }
        
        

        
        public function product_details(){
            $id = $this->uri->segment(4);
            $data['products'] = $this->mdl_products->get_certain_products_prices($id);
            $data['images'] = $this->mdl_products->get_images($id);
            $data['prices'] = $this->mdl_products->get_prices($id);
            $data['upseller'] = $this->mdl_products->get_products_inorder('counts',6);
            $data['related'] = $this->mdl_products->get_related_products($id);
            $data['carts']  = $this->cart->contents();
            $data['total'] = $this->cart->total();
            $data['content'] = 'front/product_details';
            $this->load->view('front/template/content',$data);
        }


        public function pagination($page,$table) {
            $path = base_url().'home/home_page/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
            $config['base_url'] = $path;
            $config['total_rows'] = $this->db->get($table)->num_rows();
            $config['per_page'] = $page;
            $this->pagination->initialize($config); 
        }

        function change(){
            $type = $this->uri->segment(3);
            if( $this->uri->segment(4) == TRUE ){
            $url = $this->uri->segment(4).'/'.$this->uri->segment(5);
            $path = base_url().'home/'.$url;
            }
            else {
                $path = base_url();
            }
            
            $this->session->set_userdata('lang',$type);
            redirect($path,"refresh");
        }
        
        public function count_items(){
            $num = $this->input->post('num'); 
            $this->mdl_products->counts($num);
        }
        public function count_views(){
            $num = $this->input->post('num'); 
            $this->mdl_post->c_views($num);
        }

        public function blog(){
            $data['posts'] = $this->mdl_post->get_all();
            $data['cats'] = $this->mdl_categories->get_all_categories_arr();
            $data['pages'] = $this->mdl_page->get_page_data();
            $data['products_num'] = $this->mdl_products->get_all_products(3);
            $data['social'] = $this->mdl_marketing->get_social();
            $data['content'] = 'front/blog';
            $this->load->view('front/template/content',$data);
        }

        
        public function subscribe(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','email','required|valid_email');
            
            if ($this->form_validation->run() == FALSE)
                    {
                return FALSE;
                    }
            else {
                $mail = $this->input->post('email');
                $this->load->model('mdl_homepage');
                $this->mdl_homepage->subscribe($mail);
                echo 'every thing is goes to be allright' . $mail ;
            }
        }

        
        
        public function send_messge(){
            
            $this->load->library('form_validation');
            if($this->input->post('sbt')){
                $this->form_validation->set_rules('msg_fname','first name','required');
                $this->form_validation->set_rules('msg_mail','email','required|valid_email');
                $this->form_validation->set_rules('msg_txt','message','required');
                if ($this->form_validation->run() == FALSE)
                    {
                    $data['content'] = 'front/contact';
                    $this->load->view('front/template/content',$data);
                    }
            else {
                $inputs = array ('msg_fname','msg_mail','msg_txt');
                $data = $this->messages->data_from_post($inputs); 
                    if($this->messages->save($data)){
                        $this->session->set_flashdata('message',"<div class='alert alert-success alert-flash'>  your message has been sent successfully </div>");
                    $data['popup'] = $this->session->flashdata('message') ;
                    }
                    $data['content'] = 'front/contact';
                    $this->load->view('front/template/content',$data);
                }
            } // IF IT POST 
        }
        
        function validate_phone($number){
            $subject =  $number ;
            $pattern =  "!^(010|012|011)[0-9]{8}$!";
            return preg_match($pattern,$subject );
        }
        
        function check_phone($number){
            if($number == "" || empty($number)){
                $this->form_validation->set_message('check_phone', 'the phone number is required');
		return FALSE;
            }
            elseif($this->validate_phone($number) == FALSE) {
                $this->form_validation->set_message('check_phone', 'the phone number is invalid');
		return FALSE;
            }
            else {
                return TRUE;
            }
        }
        
 
        public function update_view(){
            $id = $this->input->post('id');
            $this->db->where('pro_id',$id);
            $times = $this->db->get('products');
            $rows = $times->result();
            foreach ($rows as $r){
                $nums = $r->counts;
            }
            $v = $nums + 1;
            $view = array('counts' =>  $v);
            $this->db->where('pro_id',$id);
            $this->db->update('products',$view);
        }
        
        
    
        
    } // end of controller 