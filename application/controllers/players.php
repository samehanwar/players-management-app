<?php

class players extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('mdl_players','players');
        $this->load->model('mdl_championships','championships');
    }
    
    public function list_players(){
        $data['champs']   = $this->championships->get();
        $data['rows'] =  $this->players->get_all();
        $data['content'] = 'admin/list_players';
        $this->load->view('admin/template/content',$data);
    }
    
    
    public function add_player(){
        $data['champs']   = $this->championships->get();
        $data['content'] = 'admin/add_player';
        $this->load->view('admin/template/content',$data);
    }
    
    public function create_player(){
        
        $inputs = array ('player_fname','player_lname','dateofbirth','occupation','gender','nationality','email','photo','address','city',
                         'state','country','contact_number2','contact_number3','contact_number4','contact_number','team','club','participate','champions','rank','localrank');
        $data = $this->players->data_from_post($inputs);    
        $images = $this->input->post('uploaded_photo');
        $champs = $this->input->post('champ');
        $champcount = $this->input->post('champcount');
        $id = $this->players->save($data);
            for($x=1;$x <= count($images);$x++){
                $imgs = array(
                    'image_name' => $images[$x-1],
                    'proimg_id' => $id
                );
                $this->db->set($imgs);
                $this->db->insert('player_images');
            }
            for($x=1;$x <= count($champs);$x++){
                $champions = array(
                    'champ_id' => $champs[$x-1],
                    'player_id' => $id,
                    'champ_count' => $champcount[$x-1]
                );
                $this->db->set($champions);
                $this->db->insert('players_championships');
            }
        $data['champs']   = $this->championships->get();
        $data['images'] = $this->players->get_images($id);
        $data['player_champs'] = $this->players->get_player_champs($id);
        $data['player_champs'] = $this->players->get_player_champions($id);
        $data['rows'] =  $this->players->get($id);
        $data['content'] = 'admin/add_player';
        $this->load->view('admin/template/content',$data);
        
    }
    
    public function edit_player(){
        $id = $this->uri->segment(3);
        if(!$_POST){
            $data['images'] = $this->players->get_images($id);
            $data['player_champs'] = $this->players->get_player_champions($id);
            $data['champs']   = $this->championships->get();
            $data['rows'] =  $this->players->get($id);
            $data['content'] = 'admin/add_player';
            $this->load->view('admin/template/content',$data);
        }
        else {
            $inputs = array ('player_fname','player_lname','dateofbirth','occupation','gender','nationality','email','photo','address','city',
                         'state','country','contact_number2','contact_number3','contact_number4','contact_number','team','club','participate','champions','rank','localrank');
            $data = $this->players->data_from_post($inputs);  
            
            $images = $this->input->post('uploaded_photo');
            $imgupdate = $this->input->post('uploaded_photo_id');
            $countnewimg = count($images) - count($imgupdate) ;
            
            $champs = $this->input->post('champ');
            $champcount = $this->input->post('champcount');
            $champId = $this->input->post('champ_with_id');
            $champnewId = count($champs) - count($champcount);
            
            $this->players->save($data,$id);
                if(isset($imgupdate)){
                    for($x=1;$x <= count($imgupdate);$x++){
                        $imgs = array(
                            'image_name' => $images[$x-1],
                        );
                        $GID = $imgupdate[$x-1];
                        $this->db->set($imgs);
                        $this->db->where('image_id',$GID);
                        $this->db->update('player_images');
                    }
                }
                if($countnewimg > 0){
                    for($m=1;$m <= $countnewimg;$m++){
                        $images = array_reverse($images);
                        $imgs = array(
                        'image_name' => $images[$m-1],
                        'proimg_id' => $id
                    );
                    $this->db->set($imgs);
                    $this->db->insert('player_images');
                    }
                }
                
                    for($x=1;$x <= count($champs);$x++){
                        $champions = array(
                            'champ_id' => $champs[$x-1],
                            'player_id' => $id,
                            'champ_count' => $champcount[$x-1]
                        );
                        $this->db->set($champions);
                        $this->db->insert('players_championships');
                    }
               
//                if($champnewId > 0){
//                    for($m=1;$m <= $champnewId;$m++){
//                        $champs = array_reverse($champs);
//                        $champions = array(
//                            'champ_id' => $champs[$x-1],
//                            'player_id' => $id,
//                            'champ_count' => $champcount[$x-1]
//                        );
//                    $this->db->set($champions);
//                    $this->db->insert('players_championships');
//                    }
//                }
            
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $data['champs']   = $this->championships->get();
            $data['images'] = $this->players->get_images($id);
            $data['player_champs'] = $this->players->get_player_champions($id);
            $data['rows'] =  $this->players->get($id);
            $data['content'] = 'admin/add_player';
            $this->load->view('admin/template/content',$data);
        }
    }
    
    public function delete_player(){
         $id = $this->uri->segment(3);
         $res = $this->players->remove($id);
         if($res == TRUE) {
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $status = $this->session->flashdata('status');
            }
        redirect('players/list_players','refresh');
    }
    
    
    public function get_filter_data(){
        if($this->input->post('search') == NULL ) {
            redirect('players/list_players');
        }
        $searchtype = $this->input->post('searchtype');
        $filter = array(
            'proname' => $this->input->post('search')
        );
        $data['rows'] = $this->players->get_filtered_data($filter,$searchtype,FALSE,$this->uri->segment(3));
        $data['results'] = count($this->players->get_filtered_data($filter,$searchtype));
        $data['content'] = 'admin/list_players';
        $this->load->view('admin/template/content',$data);
    }

    
    public function show_profile(){
        $id = $this->uri->segment(3);
        $data['rows'] =  $this->players->get($id);
        $data['images'] = $this->players->get_images($id);
        $data['player_champs'] = $this->players->get_player_champions($id);
        $data['content'] = 'admin/player_profile';
        $this->load->view('admin/template/content',$data);
    }
    
    public function delete_gallery_image(){
        $id = $this->input->post('id');
        $this->players->remove_image($id,'player_images');
        $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
        return $status = $this->session->flashdata('status');
    }
    
    public function delete_champ_field(){
        $id = $this->input->post('id');
        $this->players->remove_champ_field($id,'players_championships');
        $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
        return $status = $this->session->flashdata('status');
    }

}
