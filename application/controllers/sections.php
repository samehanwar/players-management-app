<?php
    class sections extends CI_Controller {
        
        
        public function __construct() {
            parent::__construct();
            $this->load->model('mdl_sections', 'sections');     
            $this->load->model('mdl_page');
            $this->load->model('mdl_post');
        }

        public function add_section(){
            $data['pages'] = $this->mdl_page->get_page_data();
            $data['content'] = 'admin/add_section';
            $this->load->view('admin/template/content',$data);
        }
        
        public function create_section() {
                $inputs = array ('sec_name','sec_target');
                $data = $this->mdl_post->data_from_post($inputs); 
                $id = $this->sections->save($data);
                $data['rows'] = $this->sections->get($id);
                $data['pages'] = $this->mdl_page->get_page_data();
                $data['content'] = 'admin/add_section';
                $this->load->view('admin/template/content',$data);
        }
        
        public function edit_section() {
                if(!$_POST){
                    $id = $this->uri->segment(3);
                    $data['rows'] = $this->sections->get($id);
                    $data['pages'] = $this->mdl_page->get_page_data();
                    $data['content'] = 'admin/add_section';
                    $this->load->view('admin/template/content',$data);  
                }
                else {
                    $id = $this->uri->segment(3);
                    $inputs = array ('sec_name','sec_target');
                    $data = $this->mdl_post->data_from_post($inputs); 
                    $this->sections->save($data,$id);
                    $data['rows'] = $this->sections->get($id);
                    $data['pages'] = $this->mdl_page->get_page_data();
                    $data['content'] = 'admin/add_section';
                    $this->load->view('admin/template/content',$data);
                }
        }
        
        public function delete_section() {
                    $this->sections->remove($this->uri->segment(3));
                    $data['pages'] = $this->mdl_page->get_page_data();
                    $this->list_sections();
           }
        
        public function list_sections() {
                    $data['rows'] = $this->sections->get_all();
                    $data['pages'] = $this->mdl_page->get_page_data();
                    $data['content'] = 'admin/list_sections';
                    $this->load->view('admin/template/content',$data);
        }
        
    }