<?php

class federations extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('mdl_federations','federations');
    }
    
    public function list_federations(){
        $data['rows'] =  $this->federations->get_all();
        $data['content'] = 'admin/list_federations';
        $this->load->view('admin/template/content',$data);
    }
    
    
    public function add_federation(){
        $data['content'] = 'admin/add_federation';
        $this->load->view('admin/template/content',$data);
    }
    
    public function create_federation(){

        $inputs = array ('fed_name','fed_about','fed_mail','fed_phone','fed_address','fed_web','photo');
        $data = $this->federations->data_from_post($inputs);    
        $id = $this->federations->save($data);
            
        $data['rows'] =  $this->federations->get($id);
        $data['content'] = 'admin/add_federation';
        $this->load->view('admin/template/content',$data);
        
    }
    
    public function edit_federation(){
        $id = $this->uri->segment(3);
        if(!$_POST){
            $data['rows'] =  $this->federations->get($id);
            $data['content'] = 'admin/add_federation';
            $this->load->view('admin/template/content',$data);
        }
        else {
           $inputs = array ('fed_name','fed_about','fed_mail','fed_phone','fed_address','fed_web','photo');
            $data = $this->federations->data_from_post($inputs);  

            $this->federations->save($data,$id);
            
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $data['rows'] =  $this->federations->get($id);
            $data['content'] = 'admin/add_federation';
            $this->load->view('admin/template/content',$data);
        }
    }
    
    public function delete_federation(){
         $id = $this->uri->segment(3);
         $res = $this->federations->remove($id);
         if($res == TRUE) {
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $status = $this->session->flashdata('status');
            }
        redirect('federations/list_federations','refresh');
    }
    
    
    public function get_filter_data(){
        if($this->input->post('search') == NULL || empty($this->input->post('search'))) {
            redirect('federations/list_federations');
        }
        $searchtype = $this->input->post('searchtype');
        $filter = array(
            'proname' => $this->input->post('search')
        );
        $data['rows'] = $this->federations->get_filtered_data($filter,$searchtype,FALSE,$this->uri->segment(3));
        $data['results'] = count($this->federations->get_filtered_data($filter,$searchtype));
        $data['content'] = 'admin/list_federations';
        $this->load->view('admin/template/content',$data);
    }

    
    public function show_profile(){
        $id = $this->uri->segment(3);
        $data['rows'] =  $this->federations->get($id);
        $data['content'] = 'admin/player_profile';
        $this->load->view('admin/template/content',$data);
    }
    

}
