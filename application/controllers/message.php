<?php

    class message extends CI_Controller {
        
        
        function __construct() {
            parent::__construct();
            $this->load->model('mdl_message','messages');
        }


        public function add_message(){
            $data['content'] = 'admin/add_message';
            $this->load->view('admin/template/content',$data);
        }
        
        
        
        public function edit_message() {
                if(!$_POST){
                    $id = $this->uri->segment(3);
                    $data['rows'] = $this->messages->get($id);
                    $data['content'] = 'admin/add_message';
                    $this->load->view('admin/template/content',$data);  
                }
                else {
                    $id = $this->uri->segment(3);
                    $inputs = array ('post_title','post_desc','post_target','post_cat','post_order','post_title_ar','post_desc_ar','post_photo');
                    $data = $this->mdl_post->data_from_post($inputs); 
                    $this->messages->save($data);
                    $data['rows'] = $this->messages->get($id);
                    $data['content'] = 'admin/add_message';
                    $this->load->view('admin/template/content',$data);
                }
        }
        
        public function show_message() {
            $id = $this->uri->segment(3);
            $data['rows'] = $this->messages->get($id);
            $data['content'] = 'admin/show_message';
            $this->load->view('admin/template/content',$data);  
        }

        public function delete_message() {
                $this->messages->remove($this->uri->segment(3));
                $this->list_message();
           }
        
        public function list_message() {
            $data['rows'] = $this->messages->get_all();
            $data['content'] = 'admin/list_message';
            $this->load->view('admin/template/content',$data);
        }

        
    }