<?php

class championships extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('mdl_championships','championships');
    }
    
    public function list_championships(){
        $data['rows'] =  $this->championships->get_all();
        $data['content'] = 'admin/list_championships';
        $this->load->view('admin/template/content',$data);
    }
    
    
    public function add_championship(){
        $data['content'] = 'admin/add_championship';
        $this->load->view('admin/template/content',$data);
    }
    
    public function create_championship(){

        $inputs = array ('champ_name','champ_country','champ_organiser','photo');
        $data = $this->championships->data_from_post($inputs);    
        $id = $this->championships->save($data);
        $data['rows'] =  $this->championships->get($id);
        $data['content'] = 'admin/add_championship';
        $this->load->view('admin/template/content',$data);
        
    }
    
    public function edit_championship(){
        $id = $this->uri->segment(3);
        if(!$_POST){
            $data['rows'] =  $this->championships->get($id);
            $data['content'] = 'admin/add_championship';
            $this->load->view('admin/template/content',$data);
        }
        else {
           $inputs = array ('champ_name','champ_country','champ_organiser','photo');
            $data = $this->championships->data_from_post($inputs);  

            $this->championships->save($data,$id);
            
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $data['rows'] =  $this->championships->get($id);
            $data['content'] = 'admin/add_championship';
            $this->load->view('admin/template/content',$data);
        }
    }
    
    public function delete_championship(){
         $id = $this->uri->segment(3);
         $res = $this->championships->remove($id);
         if($res == TRUE) {
            $this->session->set_flashdata('status', '<p> your item is successfully deleted ! </p>'); 
            $status = $this->session->flashdata('status');
            }
        redirect('championships/list_championships','refresh');
    }
    
    

    public function get_filter_data(){
        if($this->input->post('search') == NULL || empty($this->input->post('search'))) {
            redirect('championships/list_championships');
        }
        $searchtype = $this->input->post('searchtype');
        $filter = array(
            'proname' => $this->input->post('search')
        );
        $data['rows'] = $this->championships->get_filtered_data($filter,$searchtype,FALSE,$this->uri->segment(3));
        $data['results'] = count($this->championships->get_filtered_data($filter,$searchtype));
        $data['content'] = 'admin/list_championships';
        $this->load->view('admin/template/content',$data);
    }

    
    public function show_profile(){
        $id = $this->uri->segment(3);
        $data['rows'] =  $this->championships->get($id);
        $data['content'] = 'admin/player_profile';
        $this->load->view('admin/template/content',$data);
    }
    

}
