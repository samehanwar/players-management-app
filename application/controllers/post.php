<?php

    class post extends CI_Controller {
        
        
        public function __construct() {
            parent::__construct();
            $this->load->model('mdl_post');
            $this->load->model('mdl_page');
            $this->load->model('mdl_sections');
            $this->load->model('mdl_categories');
        }

        public function add_post(){
            $data['page'] = $this->mdl_page->get_page_data();
            $data['cat'] =  $this->mdl_sections->get_all();
            $data['content'] = 'admin/add_post';
            $this->load->view('admin/template/content',$data);
        }
        
        public function create_post() {
            if($this->input->post('sb')){
                $inputs = array ('post_title','post_desc','post_target','post_cat','post_order','post_title_ar','post_desc_ar','post_photo');
                $images = $this->input->post('uploaded_photo');
                $countimg = count($images);
                $data = $this->mdl_post->data_from_post($inputs);    
                $id = $this->mdl_post->save($data);
                for($x=1;$x<=$countimg;$x++){
                        $imgs = array(
                            'image_name' => $images[$x-1],
                            'proimg_id' => $id
                        );
                    $this->db->set($imgs);
                    $this->db->insert('post_images');
                }
                    $this->get_list_view($id);
            }
            else{
                $this->get_list_view();
            }
        }
        
        
        public function edit_post() {
            
                if(!$_POST){
                    $id = $id = $this->uri->segment(3); 
                    $this->get_list_view($id);
                }
                else {
                    $inputs = array ('post_title','post_desc','post_target','post_cat','post_order','post_title_ar','post_desc_ar','post_photo');
                    
                    $images = $this->input->post('uploaded_photo');
                    $countimg = count($images);
                    $imgupdate = $this->input->post('uploaded_photo_id');
                    $imgs_with_ids = count($imgupdate);
                    $countnewimg = count($images) - count($imgupdate) ;
                    $update = $this->input->post('update_id');
                    
                    $upid = $this->uri->segment(3);
                    $data = $this->mdl_post->data_from_post($inputs);
                    $id = $this->mdl_post->save($data,$upid);
                    echo $id .'this is a segment id ' ;
                    if(isset($imgupdate)){
                    for($x=1;$x<=$imgs_with_ids;$x++){
                        $imgs = array(
                            'image_name' => $images[$x-1],
                        );
                        $GID = $imgupdate[$x-1];
                        $this->db->set($imgs);
                        $this->db->where('image_id',$GID);
                        $this->db->update('post_images');
                    }
                    }
                    if($countnewimg > 0){
                        for($m=1;$m<=$countnewimg;$m++){
                            $images = array_reverse($images);
                            $imgs = array(
                            'image_name' => $images[$m-1],
                            'proimg_id' => $upid
                        );
                        $this->db->set($imgs);
                        $this->db->insert('post_images');
                        }
                    }
                    $this->get_list_view($upid);
                }
            }
           
            
            public function get_list_view($id = NULL){
                if(!is_null($id)){
                    (int)$id;
                    $data['rows'] = $this->mdl_post->get_post($id);
                    $data['images'] = $this->mdl_post->get_images($id);
                    $data['page'] = $this->mdl_page->get_page_data();
                    $data['cat'] =  $this->mdl_sections->get_all();
                }
                    $data['page'] = $this->mdl_page->get_page_data();
                    $data['cat'] =  $this->mdl_sections->get_all();
                    $data['content'] = 'admin/add_post';
                    $this->load->view('admin/template/content',$data);
             }
        
            function delete_product_image(){
                $id = $this->input->post('id');
                $this->db->where('image_id',$id);
                $this->db->delete('post_images');
                return TRUE;
            }
        
           public function delete_post() {
                $res = $this->mdl_post->remove($this->uri->segment(3));
                if($res == TRUE) {
                    $message = $this->session->set_flashdata('$status', '<p> your item is successfully deleted ! </p>'); 
                    $this->list_post($message);
                }
           }
           
            public function pagination() {
                 $this->load->library('pagination');
                 $path = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
                 $config['base_url'] = $path;
                 $config['total_rows'] = $this->mdl_post->count_post();
                 $config['per_page'] = 10;
                 $this->pagination->initialize($config); 
            }

           public function list_post ($message = null) {
                $this->pagination();
                $data['rows'] = $this->mdl_post->get_all(10,$this->uri->segment(3));
                $data['pages'] = $this->mdl_page->get_page_data();
                $data['sects'] =  $this->mdl_sections->get_all();
                $data['status'] = $message ? $message : null;
                $data['content'] = 'admin/list_post';
                $this->load->view('admin/template/content',$data);
           }
           
           
        public function get_filter_data(){
            $filter = array(
                'proname' => $this->input->post('search')
            );

            $data['rows'] = $this->mdl_post->get_filtered_data($filter,10,$this->uri->segment(3));
            $data['cats'] = $this->mdl_categories->get_all_categories();
            $data['results'] = count($this->mdl_post->get_filtered_data($filter));
            $data['pages'] = $this->mdl_page->get_page_data();
            $data['sects'] =  $this->mdl_sections->get_all();
            $data['content'] = 'admin/list_post';
            $this->load->view('admin/template/content',$data);
        }

            public function show_post(){
            $data['content'] = 'admin/show_post';
            $this->load->view('admin/template/content',$data);
        }
        
    }