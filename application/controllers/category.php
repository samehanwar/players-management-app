<?php

    class category extends CI_Controller {
        
        public function __construct() {
            parent::__construct();
            $this->load->model('mdl_categories');
            $this->load->library('pagination');
        }
        
        
        public function list_category(){
            $this->pagination(10,'category');
            $data['rows'] = $this->mdl_categories->get_all_categories(10,$this->uri->segment(3));
            $data['content'] = 'admin/list_categories';
            $this->load->view('admin/template/content',$data);
        }

        public function add_category() {
            if($_POST){
                $data = array(
                    'catname' => $this->input->post('cat_name'),
                    'catdesc' => $this->input->post('cat_desc'),
                    'catnamear' => $this->input->post('cat_name_ar'),  
                    'catdescar' => $this->input->post('cat_desc_ar')
                );
                
                $id = $this->mdl_categories->insert_category($data);
                $data['rows'] = $this->mdl_categories->get_certain_category($id);
                $data['content'] = 'admin/add_category';
                $this->load->view('admin/template/content',$data);
            }
            else{
                $data['content'] = 'admin/add_category';
                $this->load->view('admin/template/content',$data);
            }
        }
        
        public function update_category(){
            if(!$_POST){
                $data['rows'] = $this->mdl_categories->get_certain_category($this->uri->segment(3));
                $data['content'] = 'admin/add_category';
                $this->load->view('admin/template/content',$data);
            }
            else{
                $data = array(
                    'id'        => $this->uri->segment(3),
                    'catname'   => $this->input->post('cat_name'),
                    'catdesc'   => $this->input->post('cat_desc'),
                    'catnamear' => $this->input->post('cat_name_ar'),  
                    'catdescar' => $this->input->post('cat_desc_ar')
                );
                $this->mdl_categories->change_category($data);
                $data['rows'] = $this->mdl_categories->get_certain_category($this->uri->segment(3));
                $data['content'] = 'admin/add_category';
                $this->load->view('admin/template/content',$data);
            }
        }
        
        public function delete_category(){
            $this->pagination(10,'category');
            $this->mdl_categories->remove_category($this->uri->segment(3));
            $data['msg'] = '<div class="well well-success"> <i class="fa fa-thumbs-up fa-2x"></i><span> products deleted successfully</span> <i class="fa fa-close fa-2x pull-right"></i></div>';
            $data['rows'] = $this->mdl_categories->get_all_categories(10,$this->uri->segment(3));
            $data['content'] = 'admin/list_categories';
            $this->load->view('admin/template/content',$data);
        }
        
        public function get_filter_data(){
                $filter = $this->input->post('search');
                $data['rows'] = $this->mdl_categories->get_filtered_data($filter,10,$this->uri->segment(3));
                $data['results'] = count($this->mdl_categories->get_filtered_data($filter));
                $data['content'] = 'admin/list_categories';
                $this->load->view('admin/template/content',$data);
            
        }
        
        public function pagination($page,$table) {
            $path = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
            $config['base_url'] = $path;
            $config['total_rows'] = $this->db->get($table)->num_rows();
            $config['per_page'] = $page;
            $this->pagination->initialize($config); 
        }
        
    }