-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2017 at 01:11 PM
-- Server version: 5.6.30
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MediaR-Bowling`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `ad_id` int(11) NOT NULL,
  `ad_username` varchar(30) NOT NULL,
  `ad_password` varchar(40) NOT NULL,
  `ad_fname` varchar(30) NOT NULL,
  `ad_lname` varchar(30) NOT NULL,
  `ad_mail` varchar(30) NOT NULL,
  `ad_photo` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`ad_id`, `ad_username`, `ad_password`, `ad_fname`, `ad_lname`, `ad_mail`, `ad_photo`, `date_created`, `status`, `role_id`) VALUES
(1, 'ahmed saber', 'admin', 'ad', 'adad00121', 'admin@gmail.com', 'quotes/gravatar.png', '2016-08-04 12:21:37', '1', 1),
(10, 'admin', 'admin', 'hassan ', 'el zeat', 'hassan_elzeat@elzeatgroup.com', 'quotes/happy-clients-03.jpg', '2016-08-04 12:05:33', '1', 1),
(13, 'bfbfbf', '123456', 'sajshjh', 'jhjahsjh', 'menna_lotfy@yahoo.com', 'quotes/gravatar.png', '2016-08-04 12:21:48', '1', 2),
(15, 'ahmed', 'admin', 'ad', 'adad 12121', 'admin@gmail.com', 'quotes/gravatar.png', '2016-08-04 12:21:58', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_desc` longtext NOT NULL,
  `cat_name_ar` varchar(100) NOT NULL,
  `cat_desc_ar` longtext NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_desc`, `cat_name_ar`, `cat_desc_ar`, `time_created`) VALUES
(2, 'matress', '', 'مراتب', '', '2016-04-28 06:05:55'),
(3, 'pillow', '', 'مخدات', '', '2016-04-28 06:06:38'),
(4, 'matress protector', '', 'واقى مرتبة ', '', '2016-04-28 06:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `championships`
--

CREATE TABLE IF NOT EXISTS `championships` (
  `champ_id` int(11) NOT NULL,
  `champ_name` varchar(100) NOT NULL,
  `champ_country` varchar(200) NOT NULL,
  `champ_organiser` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `championships`
--

INSERT INTO `championships` (`champ_id`, `champ_name`, `champ_country`, `champ_organiser`, `photo`) VALUES
(1, 'البطولة العربية للناشين للبولينج ', 'مصر ', 'الإتحاد المصرى للبولينج', ''),
(2, 'البطولة الدولية للناشئين', 'مصر ', 'الاتحاد المصرى للبولينج', ''),
(3, 'anything ', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `federations`
--

CREATE TABLE IF NOT EXISTS `federations` (
  `fed_id` int(11) NOT NULL,
  `fed_name` varchar(100) NOT NULL,
  `fed_about` text NOT NULL,
  `photo` varchar(200) NOT NULL,
  `fed_phone` varchar(20) NOT NULL,
  `fed_mail` varchar(50) NOT NULL,
  `fed_web` varchar(30) NOT NULL,
  `fed_address` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `federations`
--

INSERT INTO `federations` (`fed_id`, `fed_name`, `fed_about`, `photo`, `fed_phone`, `fed_mail`, `fed_web`, `fed_address`) VALUES
(2, 'الاتحاد القطرى', '  الاتحاد القطري للبولينج هو السلطة الحاكمة وطنية لالبولينج الرياضة ومعترف بها من قبل اللجنة الأولمبية الأهلية القطرية. وعلاوة على ذلك، يتم التعرف QBF أيضا من قبل المنظمات البولينج الدولية الأخرى مثل الاتحاد العالمي القارورة العشرية البولينج (WTBA)، اتحاد البولينج آسيا (ABF)، والاتحاد البولينج القارورة العشرية الأوروبي (ETBF).                                                                                                                                                                ', 'header.jpg', ' +974-44944861', 'qatarbowlingfederation@gmail.com', 'http://www.qatarbowlingfederat', '                                                                                                 Al Bidda Tower\r\nDoha\r\nQatar                                                                                                                                                                                                  ');

-- --------------------------------------------------------

--
-- Table structure for table `manufacture`
--

CREATE TABLE IF NOT EXISTS `manufacture` (
  `manuf_id` int(11) NOT NULL,
  `manuf_name` varchar(100) NOT NULL,
  `manuf_name_ar` varchar(100) NOT NULL,
  `featured` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacture`
--

INSERT INTO `manufacture` (`manuf_id`, `manuf_name`, `manuf_name_ar`, `featured`) VALUES
(1, 'high sleep', 'هاى سليب', 1),
(2, 'habitat', 'هابيتات', 1),
(3, 'sleep tight', 'سليب تايت', 1),
(4, 'janssen', 'يانسن', 0),
(5, 'spring air', 'سبرينج أير ', 0),
(6, 'englander', 'انجلندر', 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `msg_id` int(11) NOT NULL,
  `msg_fname` varchar(200) NOT NULL,
  `msg_lname` varchar(100) NOT NULL,
  `msg_txt` longtext NOT NULL,
  `msg_mail` varchar(100) NOT NULL,
  `msg_phone` int(18) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msg_id`, `msg_fname`, `msg_lname`, `msg_txt`, `msg_mail`, `msg_phone`, `time_created`) VALUES
(1, 'mohame ', 'behery', 'what an awesome website created ! ', 'mediaregypt@gmail.com', 1156987216, '2016-07-20 16:14:57'),
(7, 'sameh', 'medir', 'anybthing goes here ', 'mediaregypt@gmail.com', 1005224898, '2016-07-20 16:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(200) NOT NULL,
  `page_title_ar` varchar(300) NOT NULL,
  `page_name` varchar(50) NOT NULL,
  `page_desc` varchar(300) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_title`, `page_title_ar`, `page_name`, `page_desc`, `time_created`) VALUES
(82, 'Attract, engage and convert more qualified  shoppers', 'عن محموعة الزيات', 'about ', '<p>a short brief about the website owner and activity&nbsp;</p>', '2016-07-19 13:31:47'),
(83, 'Some things we do. This is not a complete list, we have a lot more to offer!', 'بعض الأشياء التي نقوم بها . هذه ليست قائمة كاملة ، لدينا الكثيرلنقدمه!', 'work', '<p>a good presentation for the events&nbsp;</p>', '2016-07-19 16:47:13'),
(86, 'provide a good articles and concepts related to ours', 'مفاهيم ومقالات متعلقة بطريقة عملنا', 'services', '<p>the contacts information of the website&nbsp;</p>', '2016-08-08 15:35:51'),
(87, 'feel free to call us any time , we will be waiting for you !', 'لا تتردد في الاتصال بنا في أي وقت  !', 'contact', '<p>the gallery of the photos and videos&nbsp;</p>', '2016-07-19 15:49:07'),
(88, 'the home page of the website ', 'الصفحة الرئيسية للموقع', 'home', '<p>the home page of the website&nbsp;</p>', '2015-05-18 15:03:24'),
(89, 'the latest news of kaha', 'أخر الأخبار عن شركة قها ', 'news', '', '2015-08-13 09:49:30'),
(90, '', '', 'project_details', '', '2016-07-19 16:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `player_id` int(11) NOT NULL,
  `player_fname` varchar(100) NOT NULL,
  `player_lname` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `dateofbirth` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `contact_number2` varchar(50) NOT NULL,
  `contact_number3` varchar(50) NOT NULL,
  `contact_number4` varchar(50) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modefied` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `player_brief` text NOT NULL,
  `club` varchar(100) NOT NULL,
  `team` varchar(100) NOT NULL,
  `participate` varchar(100) NOT NULL,
  `champions` varchar(100) NOT NULL,
  `rank` varchar(10) NOT NULL,
  `localrank` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`player_id`, `player_fname`, `player_lname`, `nationality`, `dateofbirth`, `gender`, `occupation`, `address`, `city`, `country`, `state`, `contact_number2`, `contact_number3`, `contact_number4`, `contact_number`, `email`, `photo`, `created`, `modefied`, `player_brief`, `club`, `team`, `participate`, `champions`, `rank`, `localrank`) VALUES
(9, 'علي عبد الرحمن والي', '', 'null', '26/7/1966', 'male', 'مدير المؤسسة المصرية لنظم الإنتخابات الإلكترونية', '16 ش اللواء صالح حرب', 'القاهرة', 'مصر', 'القاهرة', '01124444817', '', '', '01124444518', 'aawali2005@gmail.com', 'ali1.jpg', '2016-12-07 10:40:40', '0000-00-00 00:00:00', '', 'الأهلي', 'مصر', '', '', '1', ''),
(10, 'اصيل الرومي', '', '', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '%D8%A7%D9%84%D8%B1%D9%88%D9%85%D9%8A.jpg', '2016-12-07 11:21:02', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(13, 'الامير محمد بن سلطان', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '%D9%85%D8%AD%D9%85%D8%AF%20%D8%A8%D9%86%20%D8%B3%D9%84%D8%B7%D8%A7%D9%86.jpg', '2016-12-08 10:20:20', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(18, 'بدر الشيخ', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '%D8%A7%D9%84%20%D8%B4%D9%8A%D8%AE.jpg', '2016-12-08 10:30:34', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(19, 'حسين السويدي', '', 'الامارات العربية المتحدة', '', 'male', '', '', '', 'الامارات', '', '', '', '', '', '', '%D8%A7%D9%84%D8%B3%D9%88%D9%8A%D8%AF%D9%8A.jpg', '2016-12-08 10:32:00', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(22, 'ياسر ابو الريش', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '%D8%A7%D8%A8%D9%88%20%D8%A7%D9%84%D8%B1%D9%8A%D8%B4.jpg', '2016-12-08 10:36:50', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(23, 'يعقوب الشطي', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '%D8%A7%D9%84%D8%B4%D8%B7%D9%8A.jpg', '2016-12-08 10:38:11', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(24, 'عمر نور', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'amrnoor.jpg', '2016-12-12 11:48:34', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(25, 'ايمن هشلم ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'AYMAN%20HASHIM.jpg', '2016-12-12 11:51:29', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(26, 'Dominic Barrett', '', 'الولايات المتحدة الأمريكية', '', 'male', '', '', '', 'الولايات المتحده الامريكيه', '', '', '', '', '', '', 'DOMINIC%206.jpg', '2016-12-12 11:53:27', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(27, 'خالد الدوسري ', '', 'قطر', '', 'male', '', '', '', 'قطر', '', '', '', '', '', '', '', '2016-12-12 11:56:16', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(28, 'يوسف الجابر ', '', 'قطر', '', 'male', '', '', '', 'قطر', '', '', '', '', '', '', '', '2016-12-12 11:57:16', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(29, 'يوسف فلاح ', '', 'البحرين', '', 'male', '', '', '', 'البحرين', '', '', '', '', '', '', 'YOUSEF%20FALAH.jpg', '2016-12-12 11:58:29', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(30, 'Richard Teece', '', 'الولايات المتحدة الأمريكية', '', 'male', '', '', '', 'الولايات المتحده الامريكيه', '', '', '', '', '', '', 'RICHARD.jpg', '2016-12-12 12:00:12', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(31, 'جاسم الصقر ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '', '2016-12-12 12:01:03', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(32, 'عبد الرحمن سويد ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '', '2016-12-12 12:01:59', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(33, 'احمد جمال ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'AHMED%20GAMAL.jpg', '2016-12-12 12:02:37', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(34, 'نايف عقاب ', '', 'الامارات العربية المتحدة', '', 'male', '', '', '', 'الامارات ', '', '', '', '', '', '', 'image.jpg', '2016-12-12 12:14:27', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(35, 'اسلام اياد ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'islam.jpg', '2016-12-12 12:16:02', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(36, 'محمد السعود ', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '', '2016-12-12 12:17:14', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(37, 'مهدي اسعد ', '', 'البحرين', '', 'male', '', '', '', 'البحرين', '', '', '', '', '', '', '', '2016-12-12 12:17:44', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(38, 'شريف عبد المنعم ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', '', '2016-12-12 12:18:42', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(39, 'حسام صبحي ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', '%D8%B5%D8%A8%D8%AD%D9%8A.jpg', '2016-12-12 12:19:55', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(40, 'محمود مظلوم ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'MAZMAZ.jpg', '2016-12-12 12:22:23', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(41, 'حسين غلوم ', '', 'البحرين', '', 'male', '', '', '', 'البحرين', '', '', '', '', '', '', '', '2016-12-12 12:23:04', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(42, 'عمر الشيمي ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'omar%20al%20shimi.jpg', '2016-12-13 12:58:03', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(43, 'فريدريك Ohrgaard', '', 'الدانمرك', '', 'male', '', '', '', 'الدنمارك', '', '', '', '', '', '', 'frederk%202.jpg', '2016-12-13 13:08:21', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(44, 'محمد جناحي ', '', 'البحرين', '', 'male', '', '', '', 'البحرين', '', '', '', '', '', '', '%D8%AC%D9%86%D8%A7%D8%AD%D9%8A.jpg', '2016-12-13 13:10:56', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(45, 'اياد الاميري ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '', '2016-12-13 13:12:01', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(46, 'بونتوس اندرسون ', '', 'السويد', '', 'male', '', '', '', 'السويد', '', '', '', '', '', '', '', '2016-12-13 13:14:31', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(47, 'سلطان المصري ', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '', '2016-12-13 13:15:18', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(48, 'باسل العنزي ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', 'bassel.jpg', '2016-12-13 13:18:08', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(49, 'مهند ابراهيم ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', 'mohanad_ibrahim_big.jpg', '2016-12-13 13:20:22', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(50, 'عادل بركي ', '', 'المملكة العربية السعودية', '', 'male', '', '', '', 'السعوديه', '', '', '', '', '', '', '', '2016-12-13 13:24:28', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(51, 'اسيل الرومي ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '', '2016-12-13 13:29:14', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(52, 'عمرو عصام ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'amr%20isam.jpg', '2016-12-13 13:30:22', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(53, 'عمار  يامين ', '', 'الأردن', '', 'male', '', '', '', 'الاردن ', '', '', '', '', '', '', 'ammar_yamin_big.jpg', '2016-12-14 07:35:40', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(54, 'علي صالح ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', 'ali_saleh_big.jpg', '2016-12-14 07:40:18', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(55, 'ناصر جمال ', '', 'قطر', '', 'male', '', '', '', 'قطر', '', '', '', '', '', '', 'nasser_jamal_alsahoti_big.jpg', '2016-12-14 07:50:27', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(56, 'فريد غبريال ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'farid_ghobrial_big.jpg', '2016-12-14 07:58:23', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(57, 'احمد مصطفي ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'ahmed_mostafa_big.jpg', '2016-12-14 08:03:40', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(58, 'طارق حلمي ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', 'tarek_helmy.jpg', '2016-12-14 10:12:53', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(59, 'حازم عبد الراشد ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'hazim.jpg', '2016-12-14 10:13:58', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(60, 'محمد الصيرفي ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', 'SARAFI.jpg', '2016-12-14 10:14:57', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(61, 'هاني الطوخي ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', 'hany%20el%20tokhy.jpg', '2016-12-14 10:16:20', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(62, 'محمود عاطف ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', 'atif.jpg', '2016-12-14 10:17:24', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(63, 'احمد شكري ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:18:59', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(64, 'محمد كمال ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:20:58', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(65, 'سعد حسن ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:22:53', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(66, 'سعيد الشيمي ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:23:51', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(67, 'مجدي حجاج ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:24:55', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(68, 'شريف الجوهري ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', 'shrif%20al%20gohary.jpg', '2016-12-14 10:26:18', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(69, 'هاني عبد المقصود ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:27:16', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(70, 'كريم هاني ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:27:55', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(71, 'عمرو خفاجي ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', '', '2016-12-14 10:28:31', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(72, 'عمرو نور ', '', 'مصر', '', 'male', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:29:08', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(73, 'طارق جمال ', '', 'مصر', '', 'male', '', '', '', 'مصر', '', '', '', '', '', '', '', '2016-12-14 10:31:24', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(74, 'سارة جمال ', '', 'مصر', '', 'female', '', '', '', 'مصر', '', '', '', '', '', '', 'sara.jpg', '2016-12-14 10:45:58', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(75, 'علياء اسماعيل ', '', 'مصر', '', 'female', '', '', '', 'مصر', '', '', '', '', '', '', 'aliaa.jpg', '2016-12-14 10:46:38', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(76, 'منه سلطان ', '', 'مصر', '', 'female', '', '', '', 'مصر', '', '', '', '', '', '', '', '2016-12-14 10:47:14', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(77, 'هاله شعراوي ', '', 'مصر', '', 'female', '', '', '', 'مصر', '', '', '', '', '', '', 'hala2.jpg', '2016-12-14 10:47:51', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(78, 'هاله خفاجي ', '', 'مصر', '', 'female', '', '', '', 'مصر', '', '', '', '', '', '', 'hala1.jpg', '2016-12-14 10:48:23', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(79, 'ميار خالد ', '', 'مصر', '', 'female', '', '', '', 'مصر ', '', '', '', '', '', '', '', '2016-12-14 10:49:08', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(80, 'علي جمال ', '', 'الكويت', '', 'male', '', '', '', 'الكويت ', '', '', '', '', '', '', '', '2016-12-14 11:02:30', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(81, 'نادر القصابجي ', '', 'أستراليا', '', 'male', '', '', '', 'استراليا ', '', '', '', '', '', '', '', '2016-12-14 11:03:15', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(82, 'عاهد الاخرس ', '', 'الأردن', '', 'male', '', '', '', 'الاردن ', '', '', '', '', '', '', '', '2016-12-14 11:04:01', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(83, 'jksdfkfjkedjq', 'dfjksdj', 'أذربيجان', '', 'male', '', '', '', '', '', '54545489898', '4564545454', '689898989', '246656565', 'amr@mediargroup.com', '', '2016-12-14 14:01:07', '0000-00-00 00:00:00', '', '', '', '', '', '757', '546'),
(84, 'jksdfkfjkedjq', 'dfjksdj', 'أذربيجان', '', 'male', '', '', '', '', '', '54545489898', '4564545454', '689898989', '246656565', 'amr@mediargroup.com', 'YOUSSEF%203.jpg', '2016-12-14 14:08:38', '0000-00-00 00:00:00', '', '', '', '', '', '757', '546'),
(85, '', '', 'null', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '2016-12-17 17:14:13', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(86, '', '', 'null', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '2016-12-22 13:40:44', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(94, 'anyone ', 'else', 'أنتيجوا وبربودا', '12545-587', 'male', 'anything', 'live in some where', 'nearby', 'famous one', 'quite', '45545', '', '', '545 ', 'mail@some.com', 'YOUSSEF%203.jpg', '2016-12-25 16:53:20', '0000-00-00 00:00:00', '', '', '', '', '45', '', ''),
(95, 'anyone ', 'else', 'أنتيجوا وبربودا', '12545-587', 'male', 'anything', 'live in some where', 'nearby', 'famous one', 'quite', '45545', '', '', '545 ', 'mail@some.com', 'YOUSSEF%203.jpg', '2016-12-25 16:54:23', '0000-00-00 00:00:00', '', '', '', '', '45', '', ''),
(96, 'lhjh', 'hfgg1', 'آروبا', '6565', 'male', 'sdas', 'sasas', 'sasa', '', 'asa', 's4242', '', '', '422424', 'dsdsf', 'YOUSSEF%203.jpg', '2016-12-25 16:56:21', '0000-00-00 00:00:00', '', '', '', '', '42', '', ''),
(97, 'lhjh', 'hfgg1', 'آروبا', '6565', 'male', 'sdas', 'sasas', 'sasa', '', 'asa', 's4242', '', '', '422424', 'dsdsf', 'YOUSSEF%203.jpg', '2016-12-25 16:56:57', '0000-00-00 00:00:00', '', '', '', '', '42', '', ''),
(98, 'lhjh', 'hfgg1', 'آروبا', '6565', 'male', 'sdas', 'sasas', 'sasa', '', 'asa', 's4242', '', '', '422424', 'dsdsf', 'YOUSSEF%203.jpg', '2016-12-25 16:57:17', '0000-00-00 00:00:00', '', '', '', '', '42', '', ''),
(99, 'lhjh', 'hfgg1', 'آروبا', '6565', 'male', 'sdas', 'sasas', 'sasa', '', 'asa', 's4242', '', '', '422424', 'dsdsf', 'YOUSSEF%203.jpg', '2016-12-25 17:18:00', '0000-00-00 00:00:00', '', '', '', '', '42', '', ''),
(100, '', '', 'null', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '2016-12-27 10:10:12', '0000-00-00 00:00:00', '', '', '', '', '', '', ''),
(101, 'cadad', '', 'null', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '2016-12-27 10:35:33', '0000-00-00 00:00:00', '', 'adada', 'dad', 'dad', '', '23', '54'),
(102, 'cadad', '', 'null', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '2016-12-27 10:48:24', '0000-00-00 00:00:00', '', 'adada', 'dad', 'dad', '', '23', '54'),
(103, 'hgfhgfh', 'hghg', 'null', '', 'male', '', 'dfgfgfg', '', '', '', '4343434', '', '', '43434', '', 'YOUSEEF%202.jpg', '2016-12-27 11:39:35', '0000-00-00 00:00:00', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `players_championships`
--

CREATE TABLE IF NOT EXISTS `players_championships` (
  `id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `champ_id` int(11) DEFAULT NULL,
  `champ_count` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `players_championships`
--

INSERT INTO `players_championships` (`id`, `player_id`, `champ_id`, `champ_count`) VALUES
(6, 99, 1, 0),
(7, 99, 2, 0),
(8, 99, 1, 0),
(9, 9, 2, 4587),
(10, 96, 2, 78),
(11, 100, 1, 56),
(12, 100, 2, 0),
(13, 101, 1, 545),
(14, 101, 2, 5420),
(15, 102, 1, 545),
(16, 102, 2, 5420),
(17, 103, 1, 4545),
(23, 103, 1, 44);

-- --------------------------------------------------------

--
-- Table structure for table `player_images`
--

CREATE TABLE IF NOT EXISTS `player_images` (
  `image_id` int(11) NOT NULL,
  `image_name` varchar(200) DEFAULT NULL,
  `proimg_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player_images`
--

INSERT INTO `player_images` (`image_id`, `image_name`, `proimg_id`) VALUES
(2, '', 18),
(3, '', 19),
(6, NULL, 22),
(7, NULL, 23),
(8, NULL, 24),
(9, NULL, 25),
(10, '', 26),
(11, NULL, 27),
(12, NULL, 28),
(13, '', 29),
(14, 'RICHARD%20TEECE.jpg', 30),
(15, NULL, 31),
(16, NULL, 32),
(17, NULL, 33),
(18, 'image%20(1).jpg', 34),
(19, 'CSBA-VpWwAA_SAA.jpg', 34),
(20, 'uae_nayef_shaker.jpg', 34),
(22, NULL, 35),
(23, NULL, 36),
(24, NULL, 37),
(25, NULL, 38),
(26, NULL, 39),
(27, NULL, 40),
(28, NULL, 41),
(29, 'DOMINIC%203.jpg', 26),
(30, 'DOMINIC%205.jpg', 26),
(31, 'DOMINIC%204.jpg', 26),
(32, 'DOMINIC%20BARRET.jpg', 26),
(33, 'YOUSSEF%203.jpg', 29),
(34, 'RICHARAD.jpg', 30),
(37, NULL, 42),
(38, NULL, 43),
(39, NULL, 44),
(40, NULL, 45),
(41, NULL, 46),
(42, NULL, 47),
(43, NULL, 48),
(44, NULL, 49),
(45, NULL, 50),
(46, NULL, 51),
(47, NULL, 52),
(48, '', 9),
(49, NULL, 53),
(50, NULL, 54),
(51, NULL, 55),
(52, NULL, 56),
(53, NULL, 57),
(54, NULL, 58),
(55, NULL, 59),
(56, NULL, 60),
(57, '', 61),
(58, '', 62),
(59, NULL, 63),
(60, NULL, 64),
(61, '', 65),
(62, NULL, 66),
(63, NULL, 67),
(64, NULL, 68),
(65, NULL, 69),
(66, NULL, 70),
(67, NULL, 71),
(68, NULL, 72),
(69, NULL, 73),
(70, '', 74),
(71, NULL, 75),
(72, NULL, 76),
(73, NULL, 77),
(74, NULL, 78),
(75, NULL, 79),
(76, NULL, 80),
(77, NULL, 81),
(78, NULL, 82),
(79, 'RICHARD.jpg', 83),
(80, 'SARAFI.jpg', 83),
(81, 'RICHARD.jpg', 84),
(84, 'SARAFI.jpg', 84),
(85, NULL, 85),
(94, 'YOUSEEF%202.jpg', 94),
(95, 'SARAFI.jpg', 94),
(96, 'YOUSEEF%202.jpg', 95),
(97, 'SARAFI.jpg', 95),
(98, 'tarek_helmy.jpg', 96),
(99, 'tarek_helmy.jpg', 97),
(100, 'tarek_helmy.jpg', 98),
(101, 'tarek_helmy.jpg', 99),
(102, 'SARAFI.jpg', 96),
(103, 'SARAFI.jpg', 96),
(104, 'SARAFI.jpg', 96),
(105, 'SARAFI.jpg', 96),
(106, 'SARAFI.jpg', 96),
(107, 'SARAFI.jpg', 96),
(108, 'SARAFI.jpg', 96),
(109, NULL, 100),
(110, 'SARAFI.jpg', 101),
(111, 'SARAFI.jpg', 102),
(118, 'YOUSEEF%202.jpg', 102),
(119, 'SARAFI.jpg', 102),
(120, 'image.jpg', 102),
(121, 'SARAFI.jpg', 103);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(300) NOT NULL,
  `post_desc` longtext NOT NULL,
  `post_target` varchar(100) NOT NULL,
  `post_cat` varchar(30) NOT NULL,
  `post_photo` varchar(200) NOT NULL,
  `post_order` int(11) NOT NULL,
  `post_title_ar` varchar(300) NOT NULL,
  `post_desc_ar` longtext NOT NULL,
  `views` int(11) DEFAULT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_title`, `post_desc`, `post_target`, `post_cat`, `post_photo`, `post_order`, `post_title_ar`, `post_desc_ar`, `views`, `time_created`) VALUES
(31, 'Introduction about the center', '<p>The opening of the International Bowling Center of the Armed Forces Day 16 /10 / 1997 As it has been the opening of the individual World Cup Bowling Tournament No. 1997/11 / 22-33) in the period from 5) With the participation of 80 countries, it is the first world championship Held in the Arab Republic of Egypt. It contains International Bowling Center of the Armed Forces on The largest bowling alley in the Arab Republic of Egypt AMF in terms of number (24) Bowling line It is held by various local, Arab and international tournaments And provide training for Egypt''s national team, as well as a school Rookies of the Union of Egyptian Bowling</p>', '82', '7', 'about/img-11.jpg', 1, 'مقدمة عن المركز', '<p style="text-align: right;">افتتاح مركز الدولي للبولينج اليوم القوات المسلحة 16/10 / 1997 فقد كان افتتاح الفردي كأس العالم بولينج بطولة رقم 1997/11 / 22-33) في الفترة من 5) بمشاركة 80 بلدا، هو بطولة العالم الأول الذي عقد في جمهورية مصر العربية. أنه يحتوي على مركز الدولي للبولينج للقوات المسلحة على أكبر البولينغ في جمهورية مصر العربية صندوق النقد العربي من حيث عدد (24) خط البولينج يقام من قبل مختلف البطولات المحلية والعربية والدولية وتوفير التدريب للفريق القومي المصري، كما كذلك المجندون الجدد مدرسة اتحاد البولينج المصري</p>', NULL, '2015-05-18 15:56:01'),
(34, 'what we can present ?', '<p>Center seeks along with bowling to offer some of the Games Entertainment in order The players training and acquisition of skills and expertise in various Games So that they can participate in the Leagues and international forums</p>', '82', '7', 'about/img-12.jpg', 2, 'ما يمكننا تقديم؟', '<p style="text-align: right;">يسعى مركز جنبا إلى جنب مع البولينج لتقديم بعض الألعاب والترفيه من أجل اللاعبون التدريب واكتساب المهارات والخبرات في مختلف الالعاب لذا يتمكنوا من المشاركة في البطولات والمحافل الدولية</p>', NULL, '2015-05-18 15:58:38'),
(41, 'feel free to call us anytime , our support will be ready to provide you with any information you want', '<p style="text-align: left;"><strong> address : </strong> 34 abdel khalek tharwat st.; - down town - cairo - egypt</p>\r\n<p style="text-align: left;"><strong> industry : </strong> kaha city - cairo - egypt</p>\r\n<p style="text-align: left;"><strong> telephone :&nbsp;</strong>02/ 23901480-23912662</p>\r\n<p style="text-align: left;"><strong> tele/fax : </strong> 002-23903487</p>\r\n<p style="text-align: left;"><strong> mail : </strong> info@kahafoodsegypt.com</p>', '87', '', '', 2, 'لا تتردد في الاتصال بنا في أي وقت، ودعمنا سيكون على استعداد لتزويدكم بأي معلومات التي تريدها', '<p style="text-align: right;"><strong>العنوان &nbsp;:&nbsp;</strong> 34 ش عبد الخالق ثروت &nbsp;- القاهرة - مصر&nbsp;</p>\r\n<p style="text-align: right;"><strong>المصنع &nbsp;:&nbsp;</strong> مدينة قها - القاهرة - مصر&nbsp;</p>\r\n<p style="text-align: right;"><strong>التليفون &nbsp;:&nbsp;</strong>&nbsp;23901480-23912662/02</p>\r\n<p style="text-align: right;"><strong>الفاكس &nbsp;:&nbsp;</strong> 23903487/02</p>\r\n<p style="text-align: right;"><a href="mailto:info@kahafoodsegypt.com">info@kahafoodsegypt.com </a>&nbsp;<strong>: &nbsp;البريد الإلكترونى</strong></p>\r\n<p style="text-align: right;">&nbsp;</p>', NULL, '2015-07-28 15:56:34'),
(43, 'Our Mission ', '<p>From the care of our youth and the upcoming delegations ,all the time we are keen on providing them with the best entertainment and sports that helps them in good sight to the future</p>', '82', '8', 'about/sunday.jpg', 2, 'مهمتنا ورسالتنا', '<p style="text-align: right;">من رعاية شبابنا والوفود القادمة، في كل وقت نحن حريصون على توفير لهم أفضل وسائل الترفيه والرياضة التي تساعدهم في الأفق جيدة للمستقبل</p>', NULL, '2015-07-28 16:49:24'),
(54, 'Entertainmenyt Games', '<p>the center is offer alot of the entertainment games such as video Games, biliard and others.</p>', '88', '13', 'home/games.png', 1, 'ألعاب ترفيهية', '<p style="text-align: right;">يعتبر مركز واحد من أكثر الأماكن الرائعة للأصدقاء وأفراد العائلة</p>', NULL, '2015-07-30 10:09:48'),
(55, 'Family landing', '<div class="content">\r\n<h3>the center is considered one of the most fabulous places for the friends and the family individuals</h3>\r\n</div>', '88', '13', 'home/family.png', 2, 'منتزة للعائلة والأصدقاء', '<p style="text-align: right;">يعتبر مركز واحد من أكثر الأماكن الرائعة للأصدقاء وأفراد العائلة</p>', NULL, '2015-07-30 10:12:12'),
(56, 'bowling game ', '<div class="content">\r\n<h3>the center is considered one of the most fabulous places for the friends and the family individuals</h3>\r\n</div>', '88', '13', 'home/bowling.png', 3, 'لعبة البولينج', '<p style="text-align: right;">يعتبر مركز واحد من أكثر الأماكن الرائعة للأصدقاء وأفراد العائلة</p>', NULL, '2015-07-30 10:13:15'),
(57, '  Jam industrial food product is prepared using different types of fruit, ', '<p>Director and the Chief Executive Officer,jointly and severally are the only authorized spokespersons</p>', '88', '19', 'strawberry.jpg', 1, 'يتم إعداد مربى المنتجات الغذائية الصناعية باستخدام أنواع مختلفة من الفاكهة،', '<p style="text-align: right;">المدير والرئيس التنفيذي، بالتكافل والتضامن هي المتحدثين باسم الوحيد المعتمد</p>', NULL, '2015-07-30 10:42:52'),
(58, '  Jam industrial food product is prepared using different types of fruit, ', '<p>&nbsp;Director and the Chief Executive Officer,jointly and severally are the only authorized spokespersons&nbsp;</p>', '88', '19', 'macbook2.png', 2, 'يتم إعداد مربى المنتجات الغذائية الصناعية باستخدام أنواع مختلفة من الفاكهة،', '<p>المدير والرئيس التنفيذي، بالتكافل والتضامن هي المتحدثين باسم الوحيد المعتمد</p>', NULL, '2015-07-30 10:44:57'),
(59, 'the bowling center to the armed forces is prepared for coming Sinai championship.', '<p>Center seeks along with bowling to offer some of the Games Entertainment in order The players training and acquisition of skills and expertise in various Games So that they can participate in the Leagues and international forums</p>', '89', '26', 'news/news-6.jpg', 1, 'تم إعداد مركز البولينج للقوات المسلحة على حضوركم بطولة سيناء', '<p style="text-align: right;">يسعى مركز جنبا إلى جنب مع البولينج لتقديم بعض الألعاب والترفيه من أجل اللاعبون التدريب واكتساب المهارات والخبرات في مختلف الالعاب لذا يتمكنوا من المشاركة في البطولات والمحافل الدولية</p>', NULL, '2015-08-19 10:24:36'),
(61, 'our branches', '<p style="text-align: right;">&nbsp;</p>\r\n<p style="text-align: right;"><strong>المكتب الإدارى الرئيس : </strong>34 ش عبد الخالق ثروت - القاهرة - مصر</p>\r\n<p style="text-align: right;"><strong>هايبر ماركت قها &nbsp;:&nbsp;</strong>على الطريق الزراعى السريع مصر &ndash; اسكندرية بمدينة قها&nbsp; وهو المنفذ الرئيسى لبيع منتجات الشركة</p>\r\n<p style="text-align: right;"><strong>.مصنع قها :</strong> وهو أكبر مصنع لدى الشركة ويقع فى مدينة قها&nbsp;</p>', '', '', '', 0, 'فروعنا', '', NULL, '2015-10-13 14:30:17'),
(62, ' hassan el zeat - CEO ', '<p>We are one of the few agents in the area that genuinely are specialists in both sales and lettings. Whether you&rsquo;re looking to sell or let your home or want to buy or rent a home, we really are the people for you to come to.</p>', '88', '21', 'quotes/happy-clients-03.jpg', 1, 'حسن الزيات - المدير التنفيذى', '<p style="text-align: right;">ونحن واحدة من عدد قليل من وكلاء في المنطقة التي هي حقا المتخصصين في كل من المبيعات والتأجير. سواء كنت تبحث لبيع أو السماح منزلك أو ترغب في شراء أو استئجار منزل، ونحن حقا شعب لك أن تأتي إلى.</p>', NULL, '2015-10-13 14:31:22'),
(63, 'about industry', '<p>It includes the production of beverages lines, jams and Alkambot, tomato sauce, canned vegetables, production of freezers, pickles and attached to a refrigerator at the highest level to save and frozen products as well as a range of workshops such as turning, blacksmithing, carpentry and auto repair shops.</p>\r\n<h4>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(2) <strong>Empty cans</strong></h4>\r\n<p>&nbsp;it was created in 1970 , and located in kaha .</p>\r\n<p>It includes the production of&nbsp;empty cans and th eprinting and covering .</p>\r\n<h4>&nbsp; &nbsp; &nbsp; &nbsp; (3) <strong>concentratiion</strong> factory</h4>\r\n<p>&nbsp;it was created in 2000, and located in kaha .</p>\r\n<p>It includes the process of products concentrations and raw fruits&nbsp; .</p>', '88', '22', 'fdgvdfgdf.jpg', 3, 'عن الصناعة', '<p style="text-align: right;">تمتلك شركة قها العديد من خطوط الأنتاج و المصانع و العمالة المدربة على اعلى مستوى&nbsp;</p>\r\n<p style="text-align: right;">ويشمل إنتاج خطوط المشروبات والمربيات ، صلصة الطماطم والخضروات المعلبة، وإنتاج المجمدات والمخللات وتعلق على الثلاجة على أعلى مستوى لانقاذ والمنتجات المجمدة وكذلك مجموعة من ورش العمل مثل تحول، والحدادة، النجارة وتصليح السيارات المحلات التجارية.</p>\r\n<p style="text-align: right;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; مصنع علب فارغة</p>\r\n<p style="text-align: right;">&nbsp; تم إنشاؤه في عام 1970، ويقع في قها.</p>\r\n<p style="text-align: right;">ويشمل إنتاج العلب الفارغة وعشر الطباعة&nbsp;&nbsp;وغطاء.</p>\r\n<p style="text-align: right;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; مصنع&nbsp;التركيزات</p>\r\n<p style="text-align: right;">&nbsp; تم إنشاؤه في عام 2000، ويقع في قها.</p>\r\n<p style="text-align: right;">وهو يتضمن عملية من المنتجات تركيزات والفواكه الطازجة.</p>', NULL, '2015-10-13 15:02:18'),
(65, 'the sixth Arabian bowling championship for the youth will be held next august', '<p>Center seeks along with bowling to offer some of the Games Entertainment in order The players training and acquisition of skills and expertise in various Games So that they can participate in the Leagues and international forums</p>', '89', '26', 'news/news-7.jpg', 3, 'سيتم عقد بطولة البولينج العربية السادسة للشباب أغسطس المقبل', '<p style="text-align: right;">يسعى مركز جنبا إلى جنب مع البولينج لتقديم بعض الألعاب والترفيه من أجل اللاعبون التدريب واكتساب المهارات والخبرات في مختلف الالعاب لذا يتمكنوا من المشاركة في البطولات والمحافل الدولية</p>', NULL, '2016-07-19 14:08:00'),
(75, 'context management', '<p>Our highly educated staff will make sure that your project will be finished on time and specified budget and desired quality standard. Services included:</p>', '86', '25', 'services/img-06.jpg', 1, 'إدارة المسابقات و البطولات', '<p style="text-align: right;">فإن موظفينا المتعلمين تعليما عاليا تأكد من أن المشروع الخاص بك وسوف يتم الانتهاء في الوقت المحدد وتحديد الميزانية والمطلوب معايير الجودة. وتشمل الخدمات:</p>', NULL, '2016-08-08 14:59:13'),
(76, 'bowling games', '<p>we can design the room that is right for you, whether it&rsquo;s a bathroom, bedroom, kitchen, study or something a bit different. We will work with you.</p>', '86', '25', 'services/news-4.jpg', 2, 'لعبة البولينج', '<p style="text-align: right;">موظفينا المتعلمين تعليما عاليا سوف نتأكد من أن المشروع الخاص بك وسوف يتم الانتهاء في الوقت المحدد وتحديد الميزانية والمطلوب معايير الجودة. وتشمل الخدمات:</p>', NULL, '2016-08-08 15:00:47'),
(78, 'events organising ', '<p>our highly educated staff will make sure that your project will be finished on time and specified budget and desired quality standard. Services included:</p>', '86', '25', 'services/img-06.jpg', 3, 'تنظيم الحفلات', '<p>موظفينا المتعلمين تعليما عاليا سوف نتأكد من أن المشروع الخاص بك وسوف يتم الانتهاء في الوقت المحدد وتحديد الميزانية والمطلوب معايير الجودة. وتشمل الخدمات:</p>', NULL, '2016-08-08 15:02:47'),
(79, 'entertainment Games', '<p>our highly educated staff will make sure that your project will be finished on time and specified budget and desired quality standard. Services included:</p>', '86', '25', 'services/img-08.jpg', 4, 'ألعاب ترفيهية', '<p style="text-align: right;">موظفينا المتعلمين تعليما عاليا سوف نتأكد من أن المشروع الخاص بك وسوف يتم الانتهاء في الوقت المحدد وتحديد الميزانية والمطلوب معايير الجودة. وتشمل الخدمات:</p>', NULL, '2016-08-08 15:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `pro_id` int(11) NOT NULL,
  `pro_name` varchar(100) NOT NULL,
  `pro_type` varchar(100) NOT NULL,
  `pro_cat` varchar(100) NOT NULL,
  `pro_size` varchar(11) NOT NULL,
  `pro_offer` varchar(30) NOT NULL,
  `pro_desc` longtext,
  `pro_name_ar` varchar(100) NOT NULL,
  `pro_desc_ar` longtext,
  `pro_photo` varchar(200) NOT NULL,
  `pro_manufacture` varchar(100) NOT NULL,
  `counts` int(11) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pro_id`, `pro_name`, `pro_type`, `pro_cat`, `pro_size`, `pro_offer`, `pro_desc`, `pro_name_ar`, `pro_desc_ar`, `pro_photo`, `pro_manufacture`, `counts`, `time_created`) VALUES
(291, 'enjoy', 'coil', 'matress', '22 cm', '', '<p>without cotton&nbsp;</p>\r\n<p>height of : 15 cm&nbsp;</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'انجوى', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', '81dSal3JaPL._SX1000_.jpg', '3', 0, '2016-05-07 04:45:07'),
(294, 'relax cotton', 'coil', 'matress', '24 cm ', '', '<p>with cotton</p>\r\n<p>height of : 15 cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'ريلاكس بالقطن', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'relax-sleeptight/Latex-Relax-Mattress-M-LR.jpg', '3', 4, '2016-06-08 07:47:25'),
(295, 'comfort', 'coil', 'matress', '26 cm', '', '<p><br />without cotton</p>\r\n<p>height of : 15 cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'كومفورت', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'comfort-sleeptight/2000_2000_3181_9755.jpg', '3', 2, '2016-07-09 04:50:39'),
(297, 'marout plus', 'medical', 'matress', '15 cm ', '', '<p>without cotton</p>\r\n<p>height of : 15 cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'ماريوت بلس', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'maroutplus-highsleep/casper-mattress-main.png', '1', 0, '2016-05-07 22:17:08'),
(298, 'fomie', 'foam', 'matress', '20 cm', '', '<p>without cotton</p>\r\n<p>height of : 15 cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'فومى', '<p style="text-align: right;">كثافة : 20 سم&nbsp;</p>\r\n<p style="text-align: right;">ارتفاع : 15 سم</p>\r\n<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.</p>\r\n<p style="text-align: right;">من جانب اثنين من 200 جنيه.</p>\r\n<p style="text-align: right;">السعر يشمل الضرائب الرسوم.</p>', 'fomie-highsleep/71%2B9J2ErM-L._SL1500_.jpg', '1', 5, '2016-07-09 22:21:18'),
(299, 'Galaxy Pocket', 'pocket', 'matress', '28 cm', '', '<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'جالكسى بوكيت', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'galaxypocket-highsleep/529513-L.jpg', '1', 2, '2016-05-23 01:49:38'),
(300, 'contract', 'coil', 'matress', '24 cm ', '', '<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'كونتراكت', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'contract/premier-pocket-sprung-heavy-contract-mattress-%5B2%5D-185-p.jpg', '2', 1, '2016-05-07 23:30:16'),
(301, 'Royal Pillowtop', 'coil', 'matress', '40 cm', '', '<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'رويال بلى توب ', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'royal-pillowtop/1327700878-6639318_full.jpg', '1', 2, '2016-05-22 02:02:06'),
(302, 'Royal Pocket ', 'pocket', 'matress', '33 cm ', '', '<p>height of : 33&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'رويال بوكيت ', '<p style="text-align: right;">ارتفاع المرتبة : 33 سم</p>\r\n<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'royal-pocket/HT1LntCFIBaXXagOFbXX.jpg', '1', 5, '2016-07-11 02:21:34'),
(303, 'Galaxy Latex', 'pocket', 'matress', '29 cm', '', '<p>height of : 29&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'جالاكسى لاتكس', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'galaxy-latex/ec400.jpg', '1', 1, '2016-05-10 05:43:18'),
(304, 'Galaxy Pillowtop', 'pocket', 'matress', '33 cm', '', '<p>height of : 33&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'جالكسى بيلو توب', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'galaxy-pillowtop/86831Rideau%20TT.jpg', '1', 1, '2016-05-10 05:35:57'),
(305, 'Perfect Pillowtop', 'coil', 'matress', '33 cm', '', '<p>height of : 33&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'برفكت بلى توب', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'perfect-pillowtop/iSeries-Vantage-Plush-500x326.png', '1', 1, '2016-05-08 05:37:06'),
(306, 'Perfect', 'coil', 'matress', '31 cm', '', '<p>height of : 31&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'برفكت', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'perfect/king-koil-picture.jpg', '1', 0, '2016-05-08 04:56:19'),
(307, 'Relax Cotton', 'coil', 'matress', '28 cm', '', '<p>height of : 28&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'ريلاكس قطن', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'relax-cotton/Signature-Sleep-13-1024x551.jpg', '1', 0, '2016-05-08 05:02:24'),
(308, 'Cotton', 'coil', 'matress', '24 cm ', '', '<p>height of : 24&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'قطن', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'cotton/KaiCottonWool2-Side.jpg', '1', 0, '2016-05-08 05:10:54'),
(309, 'Gold Stars', 'coil', 'matress', '22 cm', '', '<p>height of : 22&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'جولد ستارز', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'goldstars/k2-_38afcf91-3ea6-4686-8ce3-1cfe5ae4e603.v2.jpg', '1', 1, '2016-05-08 06:42:21'),
(310, 'Gold time', 'coil', 'matress', '26 cm', '', '<p>height of : 26&nbsp;cm</p>\r\n<p>additional fee : minus or plus 1 cm - costs the price of the form from one side 100 EGP .</p>\r\n<p>from two sided 200EGP .</p>\r\n<p>price include taxes fee.</p>', 'جولد تايم', '<p style="text-align: right;">رسوم إضافية: زائد أو ناقص 1 سم - يكلف سعر النموذج من جانب واحد 100 جنيه.<br />من جانب اثنين من 200 جنيه.<br />السعر يشمل الضرائب الرسوم.</p>', 'goldtime/k2-_c150a8f9-7759-4b8f-9ebf-4c816aca9e22.v3.jpg', '1', 0, '2016-05-08 05:29:00'),
(311, 'enjoy', 'coil', 'pillow', 'erer', 'rere', '', 'جولد ستارز', '', 'royal-pillowtop/yhst-98360661644784_2130_12211462.jpg', '4', 0, '2016-05-19 04:35:00'),
(312, 'enjoy', 'coil', 'pillow', 'erer', 'rere', '', 'جولد ستارز', '', 'royal-pillowtop/yhst-98360661644784_2130_12211462.jpg', '4', 0, '2016-05-19 04:36:09'),
(313, 'enjoy', 'coil', 'pillow', 'erer', 'rere', '', 'جولد ستارز', '', 'royal-pillowtop/yhst-98360661644784_2130_12211462.jpg', '4', 0, '2016-05-19 04:36:31'),
(314, 'Body Rest', 'pocket', 'matress', '20 cm', '', '<p>-&nbsp;atypical matress with corner angles &nbsp;COST : 846.00 LE.</p>\r\n<p>-&nbsp;atypical matress with semi-oval shape&nbsp; COST : 1066.20&nbsp;LE.</p>\r\n<p>-&nbsp;atypical matress with circular shape&nbsp; COST : 1294.27&nbsp;LE.</p>', 'بودى ريست', '<p style="text-align: right;">- المرتبة غير نمطى&nbsp;مع زوايا زاوية التكلفة: 846.00 جنيه.<br />- المرتبة غير نمطى مع شبه بيضاوية الشكل التكلفة: 1066.20 جنيه.<br />- المرتبة&nbsp;غير نمطية مع دائرية الشكل التكلفة: 1294.27 جنيه.</p>', 'bodyrest-habitat/Continental-Sleep-Body-Rest-10-Pillowtop-Eurotop-Medium-Plush-Mattress-with-8-Box-Spring-Foundation-for-Mattress-Queen-0.jpg', '2', 0, '2016-05-20 22:54:18'),
(315, 'Royal ', 'coil', 'matress', '37 cm', '', '<p>- atypical matress with corner angles COST :1021.26 LE.</p>\r\n<p>- atypical matress with semi-oval shape COST : not industry</p>\r\n<p>- atypical matress with circular shape COST : not industry.</p>', 'رويال', '<p style="text-align: right;">- المرتبة غير نمطى مع زوايا زاوية التكلفة: 1021.26&nbsp;جنيه.<br />- المرتبة غير نمطى مع شبه بيضاوية الشكل التكلفة: لا تصنع&nbsp;.<br />- المرتبة غير نمطية مع دائرية الشكل التكلفة: لا تصنع&nbsp;</p>', 'royal-%20habitat/CliftonRoyale_2.jpg', '2', 0, '2016-05-20 23:12:53'),
(318, 'Comfort Pillow', 'coil', 'matress', '29 cm', '', '<p>- atypical matress with corner angles COST :875.21&nbsp;LE.</p>\r\n<p>- atypical matress with semi-oval shape COST : not industry</p>\r\n<p>- atypical matress with circular shape COST : not industry.</p>', 'كمفورت بيلو', '<p style="text-align: right;">- المرتبة غير نمطى مع زوايا زاوية التكلفة: &nbsp;875.21 جنيه.<br />&nbsp;المرتبة غير نمطى مع شبه بيضاوية الشكل التكلفة: لا تصنع.<br />- المرتبة غير نمطية مع دائرية الشكل التكلفة:&nbsp;لا تصنع</p>', 'comfort-pillow-habitat/2000_2000_3181_9755.jpg', '2', 2, '2016-05-24 00:54:25'),
(319, 'Platinum ', 'coil', 'matress', '23 cm', '', '<p>- atypical matress with corner angles COST :524.37&nbsp;LE.</p>\r\n<p>- atypical matress with semi-oval shape COST :&nbsp;733.43&nbsp;LE.</p>\r\n<p>- atypical matress with circular shape COST :&nbsp;957.50&nbsp;LE.</p>', 'بلاتينيوم ', '<p style="text-align: right;">- المرتبة غير نمطى مع زوايا زاوية التكلفة: &nbsp;524.37 جنيه.<br />- المرتبة غير نمطى مع شبه بيضاوية الشكل التكلفة: 733.43&nbsp;جنيه.<br />- المرتبة غير نمطية مع دائرية الشكل التكلفة: 957.50&nbsp;جنيه</p>', 'platinum-habitat/128544.jpg', '2', 0, '2016-05-21 00:13:09'),
(320, 'air visco', 'choose product type', 'pillow', '', '', '', 'اير فيسكو', '', 'air%20visco-%20high%20sleep/download.jpg', '1', 0, '2016-05-21 01:09:14'),
(321, 'Fiber pillow', 'choose product type', 'pillow', '', '', '<p>different weight according to pillow size</p>', 'مخدات فايبر ', '<p style="text-align: right;">أوزان مختلفة تبعاً لحجم المخدة&nbsp;</p>', 'fiberpillow-habitat/photo%2Bof%2Bhotel%2Bcluster%2Bfiber%2Bpillow.jpg', '2', 1, '2016-05-29 02:33:37'),
(322, 'Medical Pillows', 'choose product type', 'pillow', '', '', '<p>diffrent weight according to pillow size</p>', 'مخدات طبية ', '<p style="text-align: right;">أوزان مختلفة طبقا لأحجام المخدات </p>', 'medicalpillows-habitat/hospital%20pillows.jpg', '2', 1, '2016-05-29 01:44:43'),
(323, 'Super Relax Pillows', 'choose product type', 'pillow', '', '', '', 'مخدات السوبر ريلاكس ', '', 'super-relax-habitat/bc818af5-5c15-4eea-9f92-7a0de989cfc8.jpg._CB326449446__SR285%2C285_.jpg', '2', 0, '2016-05-21 15:13:21'),
(324, 'matress protector', 'choose product type', 'matress protector', '', '', '<p>the price of the atypical meter : 79.86 LE.</p>', 'واقى مرتبة ', '<p style="text-align: right;">سعر المتر الغير نمطى : 79.86 جنيه</p>', 'matress-protector-habitat/mattress-protector-2.jpg', '2', 0, '2016-05-21 15:24:26'),
(325, 'matress protector', 'choose product type', 'matress protector', '', '', '', 'واقى مرتبة', '', 'protector-sleeptight/sleep-defense-system-bed-bug-proof-mattress-encasement_3.jpg', '3', 1, '2016-05-29 02:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `type_id` int(11) NOT NULL,
  `pro_type_name` varchar(100) NOT NULL,
  `pro_type_name_ar` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`type_id`, `pro_type_name`, `pro_type_name_ar`) VALUES
(1, 'medical', 'طبى'),
(2, 'foam', 'اسفنج'),
(3, 'coil', 'سوست'),
(4, 'pocket', 'بوكت');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'editor'),
(3, 'subscriber');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `sec_id` int(6) NOT NULL,
  `sec_name` varchar(30) NOT NULL,
  `sec_target` varchar(50) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`sec_id`, `sec_name`, `sec_target`, `time_created`) VALUES
(7, 'about company', 'about', '2015-07-29 12:03:31'),
(8, 'our mession', 'about', '2016-08-07 10:08:53'),
(9, 'our brief', 'about', '2016-08-07 10:09:14'),
(12, 'featured_products', 'home', '2015-07-30 09:18:06'),
(13, 'presentation', 'home', '2015-07-30 09:18:39'),
(19, 'latest-news', 'home', '2015-07-30 09:19:17'),
(20, 'category-tabs', 'home', '2015-07-30 09:20:01'),
(21, 'quotes', 'home', '2016-07-20 13:49:59'),
(22, 'industry', 'home', '2015-10-13 14:54:13'),
(23, 'for-sale', 'work', '2016-07-20 14:21:52'),
(24, 'for-rent', 'work', '2016-07-20 14:22:56'),
(25, 'projects', 'services', '2016-08-08 15:25:32'),
(26, 'news_blogs', 'news', '2016-07-20 16:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `social_id` int(11) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `google` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `soundcloud` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`social_id`, `facebook`, `twitter`, `youtube`, `instagram`, `google`, `linkedin`, `soundcloud`) VALUES
(1, 'https://www.facebook.com/', 'https://twitter.com/', 'http://www.youtube.com/', 'http://instagram.com/', 'https://plus.google.com/', 'https://www.linkedin.com/', 'https://soundcloud.com/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`ad_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `championships`
--
ALTER TABLE `championships`
  ADD PRIMARY KEY (`champ_id`);

--
-- Indexes for table `federations`
--
ALTER TABLE `federations`
  ADD PRIMARY KEY (`fed_id`);

--
-- Indexes for table `manufacture`
--
ALTER TABLE `manufacture`
  ADD PRIMARY KEY (`manuf_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`player_id`);

--
-- Indexes for table `players_championships`
--
ALTER TABLE `players_championships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player_id` (`player_id`),
  ADD KEY `champ_id` (`champ_id`);

--
-- Indexes for table `player_images`
--
ALTER TABLE `player_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `proimg_id` (`proimg_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`sec_id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`social_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `championships`
--
ALTER TABLE `championships`
  MODIFY `champ_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `federations`
--
ALTER TABLE `federations`
  MODIFY `fed_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `manufacture`
--
ALTER TABLE `manufacture`
  MODIFY `manuf_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `player_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `players_championships`
--
ALTER TABLE `players_championships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `player_images`
--
ALTER TABLE `player_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `sec_id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `social_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

--
-- Constraints for table `players_championships`
--
ALTER TABLE `players_championships`
  ADD CONSTRAINT `pc2_fk` FOREIGN KEY (`champ_id`) REFERENCES `championships` (`champ_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pc_fk` FOREIGN KEY (`player_id`) REFERENCES `players` (`player_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player_images`
--
ALTER TABLE `player_images`
  ADD CONSTRAINT `pla_fk` FOREIGN KEY (`proimg_id`) REFERENCES `players` (`player_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
